#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <sampler.h>

#include <af.h>

communicationChannel_t *channel;

void close_sample(int sig) {

	close_channel(channel);

	exit(0);
}

int main(int argc, char *argv[]) {
	char s[50];
	int ret;
	hal_data_u *sample_ring_enable;
	hal_pin_t *sample_ring_enable_pin;

	if (argc > 1) {

		sample_channel_enable(true);

		open_sample_channel(&channel, O_RDONLY);
		sample_channel_set_freq(1000);

		while (1) {
			ret = read_sample(channel, s, 50);
			if (ret)
				printf("%s\n", s);
			//else
			//	printf("%c", 'E'); /* Empty ring */
		}
		close_channel(channel);
		exit(0);
	}

	signal(SIGUSR1, close_sample);
	sample_ring_enable_pin = pin_find_by_name(SAMPLE_CHANNEL_RING_ENABLE_PIN);

	printf("### ring enable pin: %x\n", sample_ring_enable_pin);

	sample_ring_enable = pin_get_value(sample_ring_enable_pin);

	printf("### sample_ring_enable: %d\n", sample_ring_enable->b);
	sample_ring_enable->b = 1;

#if 0
    open_sample_channel(&channel, O_WRONLY);

 
    while (1) {
	    ret = write_sample(channel, "1", 2);
	    if (!ret)
		    printf("%c", 'F'); /* Full ring */

	    ret = write_sample(channel, "12", 3);
	    if (!ret)
   		    printf("%c", 'F'); /* Full ring */

	    ret = write_sample(channel, "123", 4);
	    if (!ret)
    		    printf("%c", 'F'); /* Full ring */

	    ret = write_sample(channel, "1234", 5);
	    if (!ret)
		    printf("%c", 'F'); /* Full ring */

	    ret = write_sample(channel, "12345", 6);
	    if (!ret)
    		    printf("%c", 'F'); /* Full ring */

    }
#endif

}
