#include <coin/ClpSimplex.hpp>
#include <coin/CoinBuild.hpp>
#include <coin/CoinHelperFunctions.hpp>
#include <coin/CoinModel.hpp>
#include <coin/CoinTime.hpp>

int main()
{
    CoinPackedMatrix matrix;
    ClpSimplex model;

    model.setLogLevel(0);

    model.loadProblem(matrix, nullptr, nullptr, nullptr, nullptr, nullptr);
    model.setPrimalTolerance(1e-6);
    model.setDualTolerance(1e-6);

    model.initialDualSolve();

    model.dual();
    return 0;
}
