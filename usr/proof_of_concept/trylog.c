
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <af.h>

#include <uapi/rtapi.h>


void close_sample(int sig) {

	if (sig == SIGUSR1)
		rtapi_log_enable(false);
	else
		rtapi_log_enable(true);

}

int main(int argc, char *argv[]) {
    char buf[80];
    int ret;

    signal(SIGUSR1, close_sample);
    signal(SIGUSR2, close_sample);

    printf("## A\n");
    rtapi_log_enable(true);
    printf("## B\n");
    while (true) {
	    ret = rtapi_log_read(buf, 80);
	    if (ret)
		    printf("%s\n", buf);
    }
    
}
