/*
 * Copyright (C) 2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/ioctl.h>

#include <opencn/uapi/opencn.h>

int main(int argc, char **argv) {

	int fd, ret;
	char cmdline[80];

	fd = open(OPENCN_CORE_DEV_NAME, O_RDWR);

	if (argc == 1) {
		printf("OpenCN logging into %s ...\n", OPENCN_LOGFILE_NAME);

		ioctl(fd, OPENCN_IOCTL_LOGFILE_ON, 0);
	} else {
		printf("OpenCN logging shutdown\n");

		ioctl(fd, OPENCN_IOCTL_LOGFILE_OFF, 0);

		/* Move the log file into its destination */
		printf("OpenCN logging: moving /var/log/opencn.log to %s\n", OPENCN_LOGFILE_NAME);
		sprintf(cmdline, "mv /var/log/opencn.log %s\n", OPENCN_LOGFILE_NAME);

		ret = system(cmdline);
		if (ret < 0)
			perror("OpenCN logging");
	}

	return 0;
}
