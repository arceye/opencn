#include "ocn.h"
#include "canon.hh"
#include <cstdarg>
#include <cstdio>
#include "config.h"
#include "../feedopt.hpp"

#include <uapi/feedopt.h>

#include <matlab_headers.h>

namespace {
	bool first_movement = true;
    double last_x = 0, last_y = 0, last_z = 0;
    double g92_offsets[CANON_AXIS_COUNT];
    double g5x_offsets[9+1][CANON_AXIS_COUNT];
    double feedrate = 0;
    double feedrate_max = 10000;
    CANON_PLANE plane = CANON_PLANE_XY;
}

const char* canon_plane_to_str(CANON_PLANE p) {
	switch(p) {
	case CANON_PLANE_XY:
	return "XY";
	case CANON_PLANE_YZ:
	return "YZ";
	case CANON_PLANE_XZ:
	return "XZ";
	case CANON_PLANE_UV:
	return "UV";
	case CANON_PLANE_VW:
	return "VW";
	case CANON_PLANE_UW:
	return "UW";
	default:
		return "!! UNKNOWN !!";
	}
}

void push_curve(CurvStruct& c) {
	switch(c.Type) {
		case CurveType_Line:
			FNLOG("%sLINE%s from %f,%f,%f to %f,%f,%f at F=%f", 
					ASCII_RED, ASCII_RESET, 
					c.P0[0], c.P0[1], c.P0[2],
					c.P1[0], c.P1[1], c.P1[2],
					c.FeedRate);
			last_x = c.P1[0];
			last_y = c.P1[1];
			last_z = c.P1[2];
			break;
		case CurveType_Helix:
			FNLOG("%sHELIX%s from %f,%f,%f to %f,%f,%f evec=%f,%f,%f theta=%f pitch=%f F=%f",
					ASCII_RED, ASCII_RESET,
					c.P0[0], c.P0[1], c.P0[2],
					c.P1[0], c.P1[1], c.P1[2],
					c.evec[0], c.evec[1], c.evec[2],
					c.theta,
					c.pitch,
					c.FeedRate
					);
			last_x = c.P1[0];
			last_y = c.P1[1];
			last_z = c.P1[2];
			break;
		default:
			FNLOG("!!! THIS SHOULD NEVER HAPPEN !!!");
			break;
	}

	c.FeedRate /= 60.0; // Convert FeedRate into mm/s

	queue_push(QueueId_GCode, c);
}

double norm(const double v[3]) {
	return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

double dot(const double u[3], const double v[3]) {
	return u[0]*v[0] + u[1]*v[1] + u[2]*v[2];
}

double dist(const double u[3], const double v[3]) {
	return sqrt((u[0]-v[0])*(u[0]-v[0]) + (u[1]-v[1])*(u[1]-v[1]) + (u[2]-v[2])*(u[2]-v[2]));
}

void DWELL(double) {DUMMY_FN;}
void SET_FEED_MODE(int, int) {DUMMY_FN;}
void MIST_ON() {DUMMY_FN;}
void MIST_OFF() {DUMMY_FN;}
void FLOOD_ON() {DUMMY_FN;}
void FLOOD_OFF() {DUMMY_FN;}
void START_SPINDLE_CLOCKWISE(int, int) {DUMMY_FN;}
void START_SPINDLE_COUNTERCLOCKWISE(int, int) {DUMMY_FN;}
void STOP_SPINDLE_TURNING(int) {DUMMY_FN;}
void SET_SPINDLE_MODE(int, double) {DUMMY_FN;}
void START_CHANGE() {DUMMY_FN;}
void ORIENT_SPINDLE(int, double, int) {DUMMY_FN;}
void WAIT_SPINDLE_ORIENT_COMPLETE(int, double) {DUMMY_FN;}

double GET_EXTERNAL_PROBE_POSITION_A() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_PROBE_POSITION_B() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_PROBE_POSITION_C() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_PROBE_POSITION_X() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_PROBE_POSITION_Y() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_PROBE_POSITION_Z() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_PROBE_POSITION_U() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_PROBE_POSITION_V() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_PROBE_POSITION_W() { DUMMY_FN; return 0.0; }



double GET_EXTERNAL_TOOL_LENGTH_AOFFSET() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_TOOL_LENGTH_BOFFSET() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_TOOL_LENGTH_COFFSET() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_TOOL_LENGTH_XOFFSET() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_TOOL_LENGTH_YOFFSET() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_TOOL_LENGTH_ZOFFSET() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_TOOL_LENGTH_UOFFSET() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_TOOL_LENGTH_VOFFSET() { DUMMY_FN; return 0.0; }
double GET_EXTERNAL_TOOL_LENGTH_WOFFSET() { DUMMY_FN; return 0.0; }

void START_SPEED_FEED_SYNCH(int, double, bool) {DUMMY_FN;}
double GET_EXTERNAL_LENGTH_UNITS() { return 0.0; }
double GET_EXTERNAL_ANGLE_UNITS() { return 0.0; }
int GET_EXTERNAL_PROBE_TRIPPED_VALUE() { return 0.0; }
void SET_MOTION_CONTROL_MODE(int, double) {DUMMY_FN;}
double GET_EXTERNAL_MOTION_CONTROL_TOLERANCE() { return 0.0; };
CANON_MOTION_MODE GET_EXTERNAL_MOTION_CONTROL_MODE() { return 0; }
void DISABLE_FEED_OVERRIDE() {DUMMY_FN;}
void ENABLE_FEED_OVERRIDE() {DUMMY_FN;}
void ENABLE_SPEED_OVERRIDE(int) {DUMMY_FN;}
void DISABLE_SPEED_OVERRIDE(int) {DUMMY_FN;}
void PROGRAM_STOP() {DUMMY_FN;}
void CHANGE_TOOL(int) {DUMMY_FN;}
void STOP_SPEED_FEED_SYNCH() {DUMMY_FN;}
int UNLOCK_ROTARY(int, int) { return 0; }
int GET_EXTERNAL_FEED_OVERRIDE_ENABLE() { return 0; }
void PALLET_SHUTTLE() {DUMMY_FN;}
void PROGRAM_END() {DUMMY_FN;}
int GET_EXTERNAL_SPINDLE_OVERRIDE_ENABLE(int) { return 0; }
void USE_TOOL_LENGTH_OFFSET(EmcPose) {DUMMY_FN;}
int LOCK_ROTARY(int, int) { return 0; }
void RIGID_TAP(int, double, double, double, double) {DUMMY_FN;}
void FINISH() {DUMMY_FN;}
void OPTIONAL_PROGRAM_STOP() {DUMMY_FN;}
void SET_TOOL_TABLE_ENTRY(int, int, EmcPose, double, double, double, int) {DUMMY_FN;}
void TURN_PROBE_ON() {DUMMY_FN;}
void TURN_PROBE_OFF() {DUMMY_FN;}
void ENABLE_ADAPTIVE_FEED() {DUMMY_FN;}
void DISABLE_ADAPTIVE_FEED() {DUMMY_FN;}
void STRAIGHT_PROBE(int, double, double, double, double, double, double, double, double, double, unsigned char) {DUMMY_FN;}
void ENABLE_FEED_HOLD() {DUMMY_FN;}
void DISABLE_FEED_HOLD() {DUMMY_FN;}

int GET_EXTERNAL_QUEUE_EMPTY() { DUMMY_FN; return 0; }
int GET_EXTERNAL_TC_REASON() { DUMMY_FN; return 0; }
int GET_EXTERNAL_DIGITAL_INPUT(int, int) { DUMMY_FN; return 0; }
double GET_EXTERNAL_ANALOG_INPUT(int, double) { DUMMY_FN; return 0.0; }
bool GET_BLOCK_DELETE() { DUMMY_FN; return false; }
void MESSAGE(char *) {DUMMY_FN;}
void CLEAR_MOTION_OUTPUT_BIT(int) {DUMMY_FN;}
void SET_AUX_OUTPUT_BIT(int) {DUMMY_FN;}
void CLEAR_AUX_OUTPUT_BIT(int) {DUMMY_FN;}
int WAIT(int, int, int, double) { DUMMY_FN; return 0; }
void SET_MOTION_OUTPUT_VALUE(int, double) {DUMMY_FN;}
void SET_AUX_OUTPUT_VALUE(int, double) {DUMMY_FN;}
void CHANGE_TOOL_NUMBER(int) {DUMMY_FN;}
void LOGOPEN(char *s) {DUMMY_FN;}
void LOGAPPEND(char *s) {DUMMY_FN;}
void LOGCLOSE() {DUMMY_FN;}
void LOG(char *) {DUMMY_FN;}
void SET_NAIVECAM_TOLERANCE(double) {DUMMY_FN;}
void SET_MOTION_OUTPUT_BIT(int) {DUMMY_FN;}
void SELECT_POCKET(int, int) {DUMMY_FN;}

void NURBS_FEED(int, std::vector<CONTROL_POINT, std::allocator<CONTROL_POINT>>, unsigned int) {DUMMY_FN;}

/**************************************************************
 *************** PARTIALY IMPLEMENTED *************************
 **************************************************************/

int GET_EXTERNAL_LENGTH_UNIT_TYPE() { 
    FNLOG("return: %d", CANON_UNITS_MM);    
    return CANON_UNITS_MM; 
}
void USE_LENGTH_UNITS(int units) {FNLOG("units: %d", units);}

void SET_XY_ROTATION(double rot) {FNLOG("rot: %f", rot);}

void SET_FEED_REFERENCE(int ref) {FNLOG("ref: %d", ref);}

int GET_EXTERNAL_AXIS_MASK() { 
    int ret = 0b111; // XYZ machine
    FNLOG("mask = %d", ret);
    return ret;
}

double GET_EXTERNAL_POSITION_A() { FNLOG("pos = 0"); return 0.0; }
double GET_EXTERNAL_POSITION_B() { FNLOG("pos = 0"); return 0.0; }
double GET_EXTERNAL_POSITION_C() { FNLOG("pos = 0"); return 0.0; }
double GET_EXTERNAL_POSITION_X() { FNLOG("pos = 0"); return 0.0; }
double GET_EXTERNAL_POSITION_Y() { FNLOG("pos = 0"); return 0.0; }
double GET_EXTERNAL_POSITION_Z() { FNLOG("pos = 0"); return 0.0; }
double GET_EXTERNAL_POSITION_U() { FNLOG("pos = 0"); return 0.0; }
double GET_EXTERNAL_POSITION_V() { FNLOG("pos = 0"); return 0.0; }
double GET_EXTERNAL_POSITION_W() { FNLOG("pos = 0"); return 0.0; }

int GET_EXTERNAL_TOOL_SLOT() { FNLOG("slot = 0"); return 0; }
double GET_EXTERNAL_FEED_RATE() { FNLOG("rate = 9000"); return 9000.0; }
int GET_EXTERNAL_FLOOD() { FNLOG("flood = 0"); return 0; }
int GET_EXTERNAL_MIST() { FNLOG("mist = 0"); return 0; }

int GET_EXTERNAL_PLANE() { FNLOG("plane = %s (%d)", canon_plane_to_str(plane), plane); return plane; }
int GET_EXTERNAL_SELECTED_TOOL_SLOT() { FNLOG("slot = -1"); return -1; }
int GET_EXTERNAL_POCKETS_MAX() { FNLOG("pockets = 1"); return 1; }
int GET_EXTERNAL_ADAPTIVE_FEED_ENABLE() { FNLOG("enabled = 0"); return 0; }
double GET_EXTERNAL_TRAVERSE_RATE() { FNLOG("rate = 9000"); return 9000.0; }
double GET_EXTERNAL_SPEED(int n) { FNLOG("n = %d, speed = 0", n); return 0.0; }

CANON_DIRECTION GET_EXTERNAL_SPINDLE(int n) { 
    FNLOG("n = %d, dir = CANON_CLOCKWISE", n); 
    return CANON_CLOCKWISE; 
}

void GET_EXTERNAL_PARAMETER_FILE_NAME(char *out, int len) {
    out[0] = 0;
    FNLOG("filename = \"\"");
}

int GET_EXTERNAL_FEED_HOLD_ENABLE() { FNLOG("hold = 1"); return 1; }

// Returns the CANON_TOOL_TABLE structure associated with the tool
// in the given pocket
extern CANON_TOOL_TABLE GET_EXTERNAL_TOOL_TABLE(int pocket) {
    FNLOG("pocket = %d", pocket);
    CANON_TOOL_TABLE tbl;
    tbl.diameter = 1;
    tbl.backangle = 0;
    tbl.frontangle = 90;
    ZERO_EMC_POSE(tbl.offset);
    tbl.pocketno = 0;
    tbl.toolno = 0;
    return tbl;
}


int GET_EXTERNAL_TC_FAULT() { 
    FNLOG("fault-pin = %d", 0);
    return 0; 
}

void SET_FEED_RATE(double rate) {
    FNLOG("rate = %f", rate);
    feedrate = rate;    
}

void STRAIGHT_TRAVERSE(int lineno, double x, double y, double z, double a, double b, double c, double u,
                              double v, double w) {
    FNLOG("lineno = %d, x = %f, y = %f, z = %f", lineno, x, y, z);
    CurvStruct curve;
    if (first_movement) {
    	last_x = x;
    	last_y = y;
    	last_z = z;
    	first_movement = false;
    } else {
		double P0[3] = {last_x, last_y, last_z};
		double P1[3] = {x,y,z};
		if (dist(P0, P1) > 1e-6) {
			ConstrLineStruct(P0, P1, feedrate_max, ZSpdMode_NN, &curve);
			push_curve(curve);
		} else {
			FNLOG("Points too close, ignored segment");
		}
    }
}

void STRAIGHT_FEED(int lineno, double x, double y, double z, double a, double b, double c, double u, double v,
                          double w) {
    FNLOG("lineno = %d, x = %f, y = %f, z = %f", lineno, x, y, z);
    CurvStruct curve;
    if (first_movement) {
        	last_x = x;
        	last_y = y;
        	last_z = z;
        	first_movement = false;
    }else {
		double P0[3] = {last_x, last_y, last_z};
		double P1[3] = {x,y,z};
		if (dist(P0, P1) > 1e-6) {
			ConstrLineStruct(P0, P1, feedrate, ZSpdMode_NN, &curve);
			push_curve(curve);
		} else {
			FNLOG("Points too close, ignored segment");
		}
    }

}


/*
 * first_end: end of arc in the first reference axis
 * second_end: end of arc in the second reference axis
 * first_axis: offset of the center of rotation in the first reference axis
 * second_axis: offset of the center of rotation in the second reference axis
 * rotation: clockwise/anticlockwise
 * axis_end_point: movement along the third axis, orthgonal to the first two, can be 0
 */
void ARC_FEED(int lineno, double first_end, double second_end, double first_axis, double second_axis,
                     int rotation, double axis_end_point, double a, double b, double c, double u, double v, double w) {
	FNLOG("lineno = %d, first_end = %f, second_end = %f\n"
		  "first_axis = %f, second_axis = %f, rotation = %d, axis_end_point = %f",
		  lineno, first_end, second_end, first_axis, second_axis, rotation, axis_end_point);
	// rotation: sign in trigonometric conventions:
	// * +1 -> couter-clockwise
 	// * -1 -> clockwise
	CurvStruct curve;

	double P0[3] = {last_x, last_y, last_z};
	double P1[3] = {0,0,0};
	double evec[3] = {0,0,0};
	double theta = 0;

	switch(plane) {
	case CANON_PLANE_XY: {
		// center of rotation
		const double xc = first_axis;
		const double yc = second_axis;

		P1[0] = first_end;
		P1[1] = second_end;
		P1[2] = axis_end_point;
		evec[2] = 1*rotation;

		/********************************************/
		/* we need to compute the angle of rotation */
		/********************************************/

		// both points relative to the center of rotation
		const double R0[] = {P0[0] - xc, P0[1] - yc, P0[2]};
		const double R1[] = {P1[0] - xc, P1[1] - yc, P1[2]};

		const double phi0 = atan2(R0[1], R0[0]);
		const double phi1 = atan2(R1[1], R1[0]);

		// clockwise
		if (rotation == -1) {
			theta = phi0 - phi1;
		} else {
			theta = phi1 - phi0;
		}

		if (theta < 0) {
			theta += 2*M_PI;
		}

		ConstrHelixStruct(P0, P1, evec, theta, axis_end_point - last_z, feedrate, ZSpdMode_NN, &curve);
		break;
	}

	default:
		FNLOG("%sERROR: Cannot handle plane %d%s", ASCII_RED, plane, ASCII_RESET);
		break;
	}
	// void ConstrHelixStruct(const double b_P0[3], const double b_P1[3], const
//    double b_evec[3], double b_theta, double b_pitch, double b_FeedRate,
//    CurvStruct *b_CurvStruct);
	push_curve(curve);
}

void SET_G92_OFFSET(double x, double y, double z, double a, double b, double c, double u, double v, double w) {
	FNLOG("x = %lf, y = %lf, z = %lf, a = %lf, b = %lf, c = %lf, u = %lf, v = %lf, w = %lf",
			x,y,z,a,b,c,u,v,w);
	g92_offsets[CANON_AXIS_X] = x;
	g92_offsets[CANON_AXIS_Y] = y;
	g92_offsets[CANON_AXIS_Z] = z;
	g92_offsets[CANON_AXIS_A] = a;
	g92_offsets[CANON_AXIS_B] = b;
	g92_offsets[CANON_AXIS_C] = c;
	g92_offsets[CANON_AXIS_U] = u;
	g92_offsets[CANON_AXIS_V] = v;
	g92_offsets[CANON_AXIS_W] = w;
}

void SET_G5X_OFFSET(int origin, double x, double y, double z, double a, double b, double c, double u, double v,
                           double w) {
	FNLOG("origin = %d, x = %lf, y = %lf, z = %lf, a = %lf, b = %lf, c = %lf, u = %lf, v = %lf, w = %lf",
				origin, x,y,z,a,b,c,u,v,w);
	g5x_offsets[origin][CANON_AXIS_X] = x;
	g5x_offsets[origin][CANON_AXIS_Y] = y;
	g5x_offsets[origin][CANON_AXIS_Z] = z;
	g5x_offsets[origin][CANON_AXIS_A] = a;
	g5x_offsets[origin][CANON_AXIS_B] = b;
	g5x_offsets[origin][CANON_AXIS_C] = c;
	g5x_offsets[origin][CANON_AXIS_U] = u;
	g5x_offsets[origin][CANON_AXIS_V] = v;
	g5x_offsets[origin][CANON_AXIS_W] = w;
}

void COMMENT(const char *str) {
	FNLOG("str = %s", str);
}

void INIT_CANON() {
	FNLOG("INIT_CANON");
	last_x = 0;
	last_y = 0;
	last_z = 0;
	first_movement = true;

#warning This should later use "Home position" values from feedopt, settable in the GUI 
}

void SELECT_PLANE(int plane) {
	FNLOG("plane = %s (%d)", canon_plane_to_str(plane), plane);
	::plane = plane;
}

void SET_SPINDLE_SPEED(int spindle, double speed) {
#warning ACTUALY_SET_SPINDLE_SPEED
	FNLOG("spindle = %d, speed = %f", spindle, speed);
}


/**************************************************************
 ******************* FULLY IMPLEMENTED ************************
 **************************************************************/


int _task = 1;
char _parameter_file_name[LINELEN] = {0};
USER_DEFINED_FUNCTION_TYPE USER_DEFINED_FUNCTION[USER_DEFINED_FUNCTION_NUM] = {0};

