/********************************************************************
 *  Copyright (C) 2019  Peter Lichard  <peter.lichard@heig-vd.ch>
 *  Copyright (C) 2019 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>

#include <uapi/lcct.h>

char *module_name = "lcct";
int ignore_sig = 0; /* used to flag critical regions */

static int devfd;

/* signal handler */
static sig_atomic_t stop;

static void quit(int sig) {
    if (ignore_sig) {
        return;
    }
    stop = 1;
}

static int lcct_connect(char *devname) {
    int ret = 0;
    lcct_connect_args_t args = {0};

    if(devname) strncpy(args.name, module_name, sizeof(args.name));

    devfd = open(devname, O_RDWR);
    if (!devfd) {
        fprintf(stderr, "%s: ERROR: failed to open %s\n", module_name, LCCT_DEV_NAME);
        return -EIO;
    }

    ret = ioctl(devfd, LCCT_IOCTL_CONNECT, &args);
    if (ret) fprintf(stderr, "%s: ERROR: Connection error (%d)\n", module_name, ret);

    return ret;
}

int main(int argc, char **argv) {
    int ret;

    printf("Starting %s component\n", module_name);

    /* register signal handlers - if the process is killed
     we need to call hal_exit() to free the shared memory */
    signal(SIGINT, quit);
    signal(SIGTERM, quit);
    signal(SIGPIPE, SIG_IGN);

    /* connect to the HAL */
    ignore_sig = 1;
    ret = lcct_connect(LCCT_DEV_NAME);
    ignore_sig = 0;

    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);

    if (ret) {
        fprintf(stderr, "%s: ERROR: Connection with kernel failed\n", module_name);
        goto out;
    }

out:
    close(devfd);
    return ret;
}
