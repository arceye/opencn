#!/bin/bash

function usage {
  echo "$0 [OPTIONS]"
  echo "  -c        Clean"
  echo "  -d        Debug build"
  echo "  -v        Verbose"
  echo "  -s        Single core"
  echo "  -h        Print this help"
}

function install_file_root {
  [ -f $1 ] && echo "Installing $1" && cp $1 ../rootfs/target/root
}

function install_directory_root {
  [ -d $1 ] && echo "Installing $1" && cp -R $1 ../rootfs/target/root
}

clean=n
debug=n
verbose=n
singlecore=n

while getopts cdhvs option
  do
    case "${option}"
      in
        c) clean=y;;
        d) debug=y;;
	    v) verbose=y;;
        s) singlecore=y;;
        h) usage && exit 1;;
    esac
  done

SCRIPT=$(readlink -f $0)
SCRIPTPATH=`dirname $SCRIPT`

if [ $clean == y ]; then
  echo "Cleaning $SCRIPTPATH/build"
  rm -rf $SCRIPTPATH/build
  exit
fi

if [ $debug == y ]; then
  build_type="Debug"
else
  build_type="Release"
fi

echo "Starting $build_type build"
mkdir -p $SCRIPTPATH/build

cd $SCRIPTPATH/build
cmake -DCMAKE_BUILD_TYPE=$build_type -DCMAKE_TOOLCHAIN_FILE=../../rootfs/host/share/buildroot/toolchainfile.cmake ..
if [ $singlecore == y ]; then
    NRPROC=1
else
    NRPROC=$((`cat /proc/cpuinfo | awk '/^processor/{print $3}' | wc -l` + 1))
fi
if [ $verbose == y ]; then
	make VERBOSE=1 -j1
else
	make -j$NRPROC
fi
cd -

# Core
install_file_root build/core/logfile

# Components
install_file_root build/components/curses_gui/curses_gui
install_file_root build/components/feedopt/feedopt
install_file_root build/components/lcct/lcct
install_file_root build/components/lcec/lcec
install_directory_root components/lcec/example
install_file_root build/components/sampler/sampler
install_file_root components/sampler/example/one-drive-sampler.hal
install_file_root build/components/streamer/streamer
install_file_root components/streamer/example/SetPoints_fast.txt
install_file_root components/streamer/example/cmd.hal
install_file_root build/components/threads/threads
install_file_root build/components/trivkins/trivkins
install_file_root build/components/timing_tests/timing_tests

# Halcmd
install_file_root build/halcmd/halcmd

# Proof_of_concept
install_file_root build/proof_of_concept/proof-of-concept
install_file_root build/proof_of_concept/cpu23_try
install_file_root build/proof_of_concept/trysample
install_file_root build/proof_of_concept/trylog

install_file_root proof_of_concept/rate.hal

# user-gui
install_file_root build/user_gui/user-gui
install_file_root build/user_gui/example-beckhoff/example-beckhoff
install_file_root build/user_gui/testing-gui/testing-gui

