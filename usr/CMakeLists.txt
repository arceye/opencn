cmake_minimum_required(VERSION 2.8.11)

project(opencn-usr LANGUAGES C CXX)

include_directories(
	include
	../linux-4.14-amp/include/opencn
)

set(CMAKE_C_FLAGS_DEBUG "-o0 -g -fno-pie")
set(CMAKE_C_FLAGS_RELEASE "-o3 -fno-pie -march=nehalem")

set(CMAKE_CXX_FLAGS_DEBUG "-o0 -g -fno-pie")
set(CMAKE_CXX_FLAGS_RELEASE "-o3 -fno-pie -march=nehalem")

add_subdirectory(lib)
add_subdirectory(core)

option(WITH_COMPONENTS "Build Components" ON)
if (WITH_COMPONENTS)
    add_subdirectory(components)
endif()

option(WITH_HALCMD "Build halcmd" ON)
if (WITH_HALCMD)
    add_subdirectory(halcmd)
endif()

option(WITH_PROOF_OF_CONCEPT "Build proof-of-concept" ON)
if (WITH_PROOF_OF_CONCEPT)
    add_subdirectory(proof_of_concept)
endif()

option(WITH_USER_GUI "Build user-gui" ON)
if (WITH_USER_GUI)
    add_subdirectory(user_gui)
endif()
