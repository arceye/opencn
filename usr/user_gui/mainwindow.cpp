#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QShortcut>

#include <cstring>
#include <fcntl.h>
#include <sys/ioctl.h>

#define MAX_MESSAGE_LEN 10000

enum TabIndex {
    Inactive,
    Homing,
    Jog,
    Stream,
    GCode
};

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _timer = new QTimer(this);

    _streamingFileDialog = new QFileDialog(this, "Select the streamer file", QString(), "*.txt");
    _gcodeFileDialog = new QFileDialog(this, "Select the ngc file", QString(), "*.ngc");

    connect(ui->actionStreaming, &QAction::triggered, _streamingFileDialog, &QDialog::open);
    connect(_streamingFileDialog, &QFileDialog::fileSelected, this, &MainWindow::streamingFileChanged);

    connect(ui->actionG_code, &QAction::triggered, _gcodeFileDialog, &QDialog::open);
    connect(_gcodeFileDialog, &QFileDialog::fileSelected, this, &MainWindow::gcodeFileChanged);

    // Homing
    connect(ui->pushButton_startHoming, SIGNAL(pressed()), &startHoming, SLOT(setTrue()));
    connect(ui->pushButton_stopHoming, SIGNAL(pressed()), &stopHoming, SLOT(setTrue()));
    // connect(ui->pushButton_touchHoming, SIGNAL(pressed()), &touchHoming, SLOT(setTrue()));

    // Spindle
    connect(ui->doubleSpinBox_speedSpindle_rpm, SIGNAL(valueChanged(double)), &speedSpindle, SLOT(setValue(double)));
    connect(ui->checkBox_activeSpindle, SIGNAL(toggled(bool)), &activeSpindle, SLOT(setValue(bool)));

    // Init spindle speed PIN at startup
    speedSpindle.setValue(ui->doubleSpinBox_speedSpindle_rpm->value());

    // Init spindle state and air PINs values with checkboxes at startup
    activeSpindle.setValue(ui->checkBox_activeSpindle->isChecked());

    // Jog axis select
    connect(ui->radioButton_XJog, SIGNAL(toggled(bool)), &JogX, SLOT(setValue(bool)));
    connect(ui->radioButton_YJog, SIGNAL(toggled(bool)), &JogY, SLOT(setValue(bool)));
    connect(ui->radioButton_ZJog, SIGNAL(toggled(bool)), &JogZ, SLOT(setValue(bool)));

    // Init Jog axis select PINs values with radiobuttons at startup
    JogX.setValue(ui->radioButton_XJog->isChecked());
    JogY.setValue(ui->radioButton_YJog->isChecked());
    JogZ.setValue(ui->radioButton_ZJog->isChecked());

    // Jog relative
    connect(ui->doubleSpinBox_incrJog_mm, SIGNAL(valueChanged(double)), &relJog, SLOT(setValue(double)));
    connect(ui->pushButton_plusJog, SIGNAL(pressed()), &plusJog, SLOT(setTrue()));
    connect(ui->pushButton_minusJog, SIGNAL(pressed()), &minusJog, SLOT(setTrue()));

    // Init rel Jog incr speed PIN at startup
    relJog.setValue(ui->doubleSpinBox_incrJog_mm->value());

    // JogAbs
    connect(ui->doubleSpinBox_absJog_mm, SIGNAL(valueChanged(double)), &absJog, SLOT(setValue(double)));
    connect(ui->pushButton_go_absJog, SIGNAL(pressed()), &goJog, SLOT(setTrue()));

    // Init abs Jog incr PIN at startup
    absJog.setValue(ui->doubleSpinBox_absJog_mm->value());

    // Jog speed
    connect(ui->doubleSpinBox_speedJog_mm_min, SIGNAL(valueChanged(double)), &speedJog, SLOT(setValue(double)));
    connect(ui->pushButton_stopJog, SIGNAL(pressed()), &stopJog, SLOT(setTrue()));

    // Init Jog speed PIN at startup
    speedJog.setValue(ui->doubleSpinBox_speedJog_mm_min->value());

    // Stream offset
    connect(ui->doubleSpinBox_offsetXStream_mm, SIGNAL(valueChanged(double)), &offsetXStream, SLOT(setValue(double)));
    connect(ui->doubleSpinBox_offsetYStream_mm, SIGNAL(valueChanged(double)), &offsetYStream, SLOT(setValue(double)));
    connect(ui->doubleSpinBox_offsetZStream_mm, SIGNAL(valueChanged(double)), &offsetZStream, SLOT(setValue(double)));

    // Home position in workspace coordinates
    connect(ui->spin_homePositionX_mm, SIGNAL(valueChanged(double)), &homePositionX, SLOT(setValue(double)));
    connect(ui->spin_homePositionY_mm, SIGNAL(valueChanged(double)), &homePositionY, SLOT(setValue(double)));
    connect(ui->spin_homePositionZ_mm, SIGNAL(valueChanged(double)), &homePositionZ, SLOT(setValue(double)));

    // Init offset stream PINs at startup
    offsetXStream.setValue(ui->doubleSpinBox_offsetXStream_mm->value());
    offsetYStream.setValue(ui->doubleSpinBox_offsetYStream_mm->value());
    offsetZStream.setValue(ui->doubleSpinBox_offsetZStream_mm->value());

    // Stream control
    connect(ui->pushButton_startStream, SIGNAL(pressed()), &startStream, SLOT(setTrue()));
    connect(ui->pushButton_stopStream, SIGNAL(pressed()), &stopStream, SLOT(setTrue()));
//    connect(ui->pushButton_reloadStream, SIGNAL(pressed()), &reloadStream, SLOT(setTrue()));


    // Fault Reset
    connect(ui->pushButton_fault, SIGNAL(pressed()), &faultReset, SLOT(setTrue()));

    // Curr Pos
    ui->doubleSpinBox_currPosX->setValue(currPosX.getValue());
    ui->doubleSpinBox_currPosY->setValue(currPosY.getValue());
    ui->doubleSpinBox_currPosZ->setValue(currPosZ.getValue());

    connect(_timer, &QTimer::timeout, this, &MainWindow::timer_update);

    _timer->setInterval(100);
    _timer->start();

    connect(ui->doubleSpinBox_offsetXStream_mm, SIGNAL(valueChanged(double)), ui->openGLWidget_toolpath->getPart(),
            SLOT(offsetXChange(double)));

    connect(ui->doubleSpinBox_offsetYStream_mm, SIGNAL(valueChanged(double)), ui->openGLWidget_toolpath->getPart(),
            SLOT(offsetYChange(double)));

    connect(ui->doubleSpinBox_offsetZStream_mm, SIGNAL(valueChanged(double)), ui->openGLWidget_toolpath->getPart(),
            SLOT(offsetZChange(double)));

    rtapi_log_enable(true);
//    this->showMaximized();
    connect(ui->push_clearpath, SIGNAL(pressed()), ui->openGLWidget_toolpath->getPath(), SLOT(reset()));
    connect(ui->push_gcodestart, SIGNAL(pressed()), &gcodeStart, SLOT(setTrue()));
    connect(ui->push_gcodepause, SIGNAL(pressed()), &gcodePause, SLOT(setTrue()));
//    connect(ui->push_gcodeprepare, SIGNAL(pressed()), &gcodePrepare, SLOT(setTrue()));
    connect(ui->push_gcodeprepare, &QPushButton::pressed, _gcodeFileDialog, &QDialog::open);

    connect(ui->push_gcodereset, SIGNAL(pressed()), &feedopt_reset, SLOT(setTrue()));


    ui->tabWidget->setCurrentIndex(TabIndex::Inactive);
    // Feedopt
    ui->progress_queue_size->setMaximum(FEEDOPT_RT_QUEUE_SIZE);

    // setup shared memory
    config_key = ftok(FEEDOPT_SHMEM_NAME, FEEDOPT_SHMEM_KEY_CONFIG);
    config_shmid = shmget(config_key, sizeof(FeedoptConfigStruct), 0666 | IPC_CREAT);
    config_mem = static_cast<FeedoptConfigStruct*>(shmat(config_shmid, nullptr, 0));
    if ((long)config_mem == -1) {
        qDebug() << "Failed to get config_mem";
        config_mem = nullptr;
    }

    curv_key = ftok(FEEDOPT_SHMEM_NAME, FEEDOPT_SHMEM_KEY_CURV);
    curv_shmid = shmget(curv_key, sizeof(SharedCurvStructs), 0666 | IPC_CREAT);
    curv_mem = static_cast<SharedCurvStructs*>(shmat(curv_shmid, nullptr, 0));
    if ((long)curv_mem == -1) {
        qDebug() << "Failed to get curv_mem";
        curv_mem = nullptr;
    }

//    ui->openGLWidget_toolpath->hide();
//    ui->frame_OpenGL->hide();
    ui->frame_Gcode->hide();

    ui->tab_streaming->hide();

    this->on_push_gcoderefresh_clicked();
    //ui->line_gcodesource->setText("source.ngc");
    //this->on_push_gcodecommit_clicked();

//    ui->push_gcodepause->grabShortcut(QKeySequence(Qt::Key_Space), Qt::ApplicationShortcut);

    QShortcut* shortcut = new QShortcut(QKeySequence(Qt::Key_Space), this);
    connect(shortcut, SIGNAL(activated()), this, SLOT(on_space_pressed()));
}

MainWindow::~MainWindow() {
    delete ui;
    shmdt(config_mem);
}

void MainWindow::streamingFileChanged(const QString &path)
{
    ui->openGLWidget_toolpath->getPart()->loadStreamingFile(path);
}

void MainWindow::gcodeFileChanged(const QString &path)
{
    ui->line_gcodesource->setText(path);
    usleep(10000);
    while(queue_size.getValue() > 0) {
        usleep(1000);
    }
    ui->openGLWidget_toolpath->getPart()->loadCurvStruct(nullptr, 0);
//    this->on_push_gcodecommit_clicked();
}

void MainWindow::timer_update()
{
    static char msg[MAX_MESSAGE_LEN];

    _timer->setInterval(1000/ui->slider_refreshrate->value());

    if (curv_mem) {
        sem_wait(&curv_mem->mutex);
        if (curv_mem->generation != current_curv_generation) {
            qDebug() << "New curv generation";
            curv_structs.resize(curv_mem->curv_struct_count);
            std::memcpy(curv_structs.data(), curv_mem->curv_structs, curv_mem->curv_struct_count*sizeof(CurvStruct));
            ui->openGLWidget_toolpath->getPart()->loadCurvStruct(curv_structs.data(), curv_structs.size());
            current_curv_generation = curv_mem->generation;
        }
        sem_post(&curv_mem->mutex);
    }

    ui->doubleSpinBox_currPosX->setValue(currPosX.getValue());
    ui->doubleSpinBox_currPosY->setValue(currPosY.getValue());
    ui->doubleSpinBox_currPosZ->setValue(currPosZ.getValue());
    ui->doubleSpinBox_currVelSpindle->setValue(currVelSpindle.getValue()*60);

    //Display X axis current Mode
    if (curr_modeCSPX.getValue()) {
        ui->label_axisXMode->setText("<font color=#00AA00>CSP</font>");
    } else if (curr_modeCSVX.getValue()) {
        ui->label_axisXMode->setText("<font color=#00AA00>CSV</font>");
    } else if (curr_modeHomingX.getValue()) {
        ui->label_axisXMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (curr_modeInactiveX.getValue()) {
        ui->label_axisXMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (curr_modeFaultX.getValue()) {
        ui->label_axisXMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_axisXMode->setText("<font color=#000000>UNKNOWN</font>");
    }

    //Display Y axis current Mode
    if (curr_modeCSPY.getValue()) {
        ui->label_axisYMode->setText("<font color=#00AA00>CSP</font>");
    } else if (curr_modeCSVY.getValue()) {
        ui->label_axisYMode->setText("<font color=#00AA00>CSV</font>");
    } else if (curr_modeHomingY.getValue()) {
        ui->label_axisYMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (curr_modeInactiveY.getValue()) {
        ui->label_axisYMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (curr_modeFaultY.getValue()) {
        ui->label_axisYMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_axisYMode->setText("<font color=#000000>UNKNOWN</font>");
    }

    //Display Z axis current Mode
    if (curr_modeCSPZ.getValue()) {
        ui->label_axisZMode->setText("<font color=#00AA00>CSP</font>");
    } else if (curr_modeCSVZ.getValue()) {
        ui->label_axisZMode->setText("<font color=#00AA00>CSV</font>");
    } else if (curr_modeHomingZ.getValue()) {
        ui->label_axisZMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (curr_modeInactiveZ.getValue()) {
        ui->label_axisZMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (curr_modeFaultZ.getValue()) {
        ui->label_axisZMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_axisZMode->setText("<font color=#000000>UNKNOWN</font>");
    }

    //Display Spindle current Mode
    if (curr_modeCSPSpindle.getValue()) {
        ui->label_SpindleMode->setText("<font color=#00AA00>CSP</font>");
    } else if (curr_modeCSVSpindle.getValue()) {
        ui->label_SpindleMode->setText("<font color=#00AA00>CSV</font>");
    } else if (curr_modeHomingSpindle.getValue()) {
        ui->label_SpindleMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (curr_modeInactiveSpindle.getValue()) {
        ui->label_SpindleMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (curr_modeFaultSpindle.getValue()) {
        ui->label_SpindleMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_SpindleMode->setText("<font color=#000000>UNKNOWN</font>");
    }

    if (is_homed.getValue()) {
        ui->label_ishomed->setText("<font color=#00AA00>HOMED</font>");
    } else {
        ui->label_ishomed->setText("<font color=#FA0000>NOT HOMED</font>");
    }

    while (rtapi_log_read(msg, MAX_MESSAGE_LEN) > 0) {
        QString str_msg{msg};
        ui->listWidget_logs->addItem(str_msg.trimmed());
        ui->listWidget_logs->scrollToBottom();
    }

    ui->tabWidget->blockSignals(true);
    if (inModeInactive.getValue()) {
        ui->tabWidget->setCurrentIndex(TabIndex::Inactive);
    }
    else if (inModeHoming.getValue()) {
        ui->tabWidget->setCurrentIndex(TabIndex::Homing);
    }
    else if (inModeStream.getValue()) {
        ui->tabWidget->setCurrentIndex(TabIndex::Stream);
    }
    else if (inModeJog.getValue()) {
        ui->tabWidget->setCurrentIndex(TabIndex::Jog);
    }
    else if(inModeGCode.getValue()) {
       ui->tabWidget->setCurrentIndex(TabIndex::GCode);
    }

    ui->tabWidget->blockSignals(false);

    ui->tabWidget->setTabEnabled(TabIndex::Stream, is_homed.getValue());
    ui->tabWidget->setTabEnabled(TabIndex::GCode, is_homed.getValue());

    ui->pushButton_go_absJog->setEnabled(is_homed.getValue());

    switch(ui->tabWidget->currentIndex()) {
    case TabIndex::Jog:
        ui->tabWidget->tabBar()->setEnabled(jogFinished.getValue());
        break;
    case TabIndex::Stream:
        ui->tabWidget->tabBar()->setEnabled(streamFinished.getValue());
        break;
    case TabIndex::Homing:
        ui->tabWidget->tabBar()->setEnabled(homingFinished.getValue());
        break;
    case TabIndex::GCode:
        ui->tabWidget->tabBar()->setEnabled(gcodeFinished.getValue());
        break;
    case TabIndex::Inactive:
        ui->tabWidget->tabBar()->setEnabled(true);
        break;
    }

    /* ================== FEEDOPT ================== */
    ui->progress_queue_size->setValue(queue_size.getValue());
    ui->spin_step_dt->setValue(feedopt_step_dt.getValue()*1000);

    ui->push_gcodestart->setEnabled(feedoptReady.getValue());
    ui->spin_current_u->setValue(current_u.getValue());
    feedrate_scale.setValue(ui->slider_feedrate_scale->value()/100.0);

    ui->openGLWidget_toolpath->update();

    ui->push_gcodeprepare->setEnabled(!feedopt_us_active.getValue() && !feedopt_rt_active.getValue());
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    // Mode Select

    switch(index) {
    case TabIndex::Inactive:
        // inactive
        modeInactive.setTrue();
        break;
    case TabIndex::Homing:
        // homing
        modeHoming.setTrue();
        break;
    case TabIndex::Stream:
        // streaming
        modeStream.setTrue();
        break;
    case TabIndex::Jog:
        // jog
        modeJog.setTrue();
        break;
    case TabIndex::GCode:
        // GCode
        modeGCode.setTrue();
        on_push_gcoderefresh_clicked();
    }
}

void MainWindow::on_push_gcodecommit_clicked()
{

    if (!config_mem)
    {
        qDebug() << "COMMIT: config_mem = 0";
        return;
    }

    qDebug() << "COMMIT: OK";
    config_mem->NHorz = ui->spin_nhorz->value();
    config_mem->NDiscr = ui->spin_ndiscr->value();
//    config_mem->SplineDegree = ui->spin_splinedegree->value();
    config_mem->NBreak = ui->spin_nbreak->value();
    config_mem->LSplit = ui->spin_lsplit->value();
    config_mem->CutOff = ui->spin_cutoff->value();
    config_mem->DebugPrint = ui->check_debugprint->isChecked();
    strncpy(config_mem->source, ui->line_gcodesource->text().toStdString().c_str(), sizeof(config_mem->source));

    commit_cfg.setTrue();
}

void MainWindow::on_push_gcoderefresh_clicked()
{
    if (!config_mem) return;

    ui->spin_nhorz->setValue(config_mem->NHorz);
    ui->spin_ndiscr->setValue(config_mem->NDiscr);
//    ui->spin_splinedegree->setValue(config_mem->SplineDegree);
    ui->spin_nbreak->setValue(config_mem->NBreak);
    ui->spin_lsplit->setValue(config_mem->LSplit);
    ui->spin_cutoff->setValue(config_mem->CutOff);
    ui->check_debugprint->setChecked(config_mem->DebugPrint);
}

void MainWindow::on_space_pressed()
{
    gcodePause.setTrue();
    stopJog.setTrue();
}
