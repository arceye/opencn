#ifndef FEEDOPTWINDOW_H
#define FEEDOPTWINDOW_H

#include <QMainWindow>
#include <QTimer>

#include <sys/ipc.h>
#include <sys/shm.h>

#include "pin.h"

extern "C" {
#include <uapi/feedopt.h>
}

namespace Ui
{
class FeedoptWindow;
}

class FeedoptWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FeedoptWindow(QWidget *parent = nullptr);
    ~FeedoptWindow();

private slots:
    void timer_update();

    void on_push_commit_clicked();

    void on_push_refresh_clicked();

private:
    Ui::FeedoptWindow *ui;
    QTimer *_timer;

    PinDouble feedopt_sample_x{"feedopt.sample-0"};
    PinDouble feedopt_sample_y{"feedopt.sample-1"};
    PinDouble feedopt_sample_z{"feedopt.sample-2"};

    PinDouble feedopt_step_dt{"feedopt.step-dt"};

//    PinBool rt_active{"feedopt.rt-active"};
//    PinBool us_active{"feedopt.us-active"};
//    PinBool rt_single_shot{"feedopt.rt-single-shot"};

    PinBool commit_cfg{"feedopt.commit-cfg"};

    PinInt32 queue_size{"feedopt.queue-size"};

    // shared memory for configuration
    key_t config_key{0};
    int config_shmid{0};
    FeedoptConfigStruct* config_mem{nullptr};
};

#endif // FEEDOPTWINDOW_H
