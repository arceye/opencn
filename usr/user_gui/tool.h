#ifndef TOOL_H
#define TOOL_H

#include <QVector3D>

class Tool {
public:
    Tool();

    void draw();

    void setPosition(const QVector3D& pos);
    void setPosition(float x, float y, float z);

private:
    QVector3D _position;

    static const float ToolRadius;
    static const float ToolHeight;
};

#endif /* TOOL_H */
