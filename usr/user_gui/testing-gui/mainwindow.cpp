#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //    mode_homing = new PinBool{"lcct.machine-mode-homing"};
    _timer = new QTimer(this);

    // HOMING
    connect(ui->button_mode_homing, SIGNAL(pressed()), &mode_homing, SLOT(setTrue()));
    connect(ui->button_start_homing, SIGNAL(pressed()), &homing_start, SLOT(setTrue()));
    connect(ui->button_stop_homing, SIGNAL(pressed()), &homing_stop, SLOT(setTrue()));

    // STREAMING
    connect(ui->button_mode_streaming, SIGNAL(pressed()), &mode_streaming, SLOT(setTrue()));
    connect(ui->button_stream_start, SIGNAL(pressed()), &streaming_start, SLOT(setTrue()));
    connect(ui->button_stream_stop, SIGNAL(pressed()), &streaming_stop, SLOT(setTrue()));

    // FEEDBACK
    connect(&target_position_x, SIGNAL(valueChanged(double)), ui->spin_target_x, SLOT(setValue(double)));
    connect(&target_position_y, SIGNAL(valueChanged(double)), ui->spin_target_y, SLOT(setValue(double)));
    connect(&target_position_z, SIGNAL(valueChanged(double)), ui->spin_target_z, SLOT(setValue(double)));

    // JOG
    connect(ui->button_mode_jog, SIGNAL(pressed()), &mode_jog, SLOT(setTrue()));
    connect(ui->button_jog_plus, SIGNAL(pressed()), &jog_plus, SLOT(setTrue()));
    connect(ui->button_jog_minus, SIGNAL(pressed()), &jog_minus, SLOT(setTrue()));
    connect(ui->radio_jog_axis_x, SIGNAL(toggled(bool)), &jog_axis_x, SLOT(setValue(bool)));
    connect(ui->radio_jog_axis_y, SIGNAL(toggled(bool)), &jog_axis_y, SLOT(setValue(bool)));
    connect(ui->radio_jog_axis_z, SIGNAL(toggled(bool)), &jog_axis_z, SLOT(setValue(bool)));
    connect(ui->spin_rel_jog, SIGNAL(valueChanged(double)), &jog_rel_step, SLOT(setValue(double)));
    connect(ui->spin_jog_velocity, SIGNAL(valueChanged(double)), &jog_velocity, SLOT(setValue(double)));

    // SPINDLE
    connect(ui->spin_spindle_speed, SIGNAL(valueChanged(double)), &spindle_speed, SLOT(setValue(double)));
    connect(ui->check_spindle_force_active, SIGNAL(toggled(bool)), &spindle_force_active, SLOT(setValue(bool)));
    // OFFSETS
    connect(ui->spin_offset_x, SIGNAL(valueChanged(double)), &offset_x, SLOT(setValue(double)));
    connect(ui->spin_offset_y, SIGNAL(valueChanged(double)), &offset_y, SLOT(setValue(double)));
    connect(ui->spin_offset_z, SIGNAL(valueChanged(double)), &offset_z, SLOT(setValue(double)));

    // FAULT
    connect(ui->button_fault_reset, SIGNAL(pressed()), &fault_reset, SLOT(setTrue()));

    connect(_timer, &QTimer::timeout, this, &MainWindow::timer_tick);

    //    QPalette pal = ui->button_stream_reload->palette();
    //    pal.setColor(QPalette::Button, QColor(Qt::red));
    //    ui->button_stream_reload->setAutoFillBackground(true);
    //    ui->button_stream_reload->setPalette(pal);
    //    ui->button_stream_reload->update();

    _timer->setInterval(30);
    _timer->start();
}

void MainWindow::timer_tick()
{
    //    qDebug() << "TIMER TICK" << endl;
    target_position_x.update();
    target_position_y.update();
    target_position_z.update();
}

MainWindow::~MainWindow() { delete ui; }
