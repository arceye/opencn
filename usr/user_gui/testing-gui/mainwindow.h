#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "pin.h"
#include <QMainWindow>
#include <QTimer>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void timer_tick();

private:
    Ui::MainWindow *ui;
    QTimer *_timer;

    // HOMING
    PinBool mode_homing{"lcct.machine-mode-homing"};
    PinBool homing_start{"lcct.start-homing-sequence"};
    PinBool homing_stop{"lcct.stop-homing-sequence"};

    // STREAMING
    PinBool mode_streaming{"lcct.machine-mode-stream"};
    PinBool streaming_start{"lcct.streamer-start-in"};
    PinBool streaming_stop{"lcct.streamer-stop-in"};
    PinBool streaming_reload{"lcct.streamer-reload-in"};

    // Position
    PinDouble target_position_x{"lcct.joint-pos-cur-in-0"};
    PinDouble target_position_y{"lcct.joint-pos-cur-in-1"};
    PinDouble target_position_z{"lcct.joint-pos-cur-in-2"};

    // JOGGING
    PinBool mode_jog{"lcct.machine-mode-jog"};
    PinBool jog_plus{"lcct.jog-plus"};
    PinBool jog_minus{"lcct.jog-minus"};
    PinBool jog_axis_x{"lcct.jog-axis-X"};
    PinBool jog_axis_y{"lcct.jog-axis-Y"};
    PinBool jog_axis_z{"lcct.jog-axis-Z"};
    PinDouble jog_rel_step{"lcct.jog-move-rel"};
    PinDouble jog_velocity{"lcct.jog-velocity"};

    // OFFSETS
    PinDouble offset_x{"lcct.spinbox-offset-X"};
    PinDouble offset_y{"lcct.spinbox-offset-Y"};
    PinDouble offset_z{"lcct.spinbox-offset-Z"};

    // SPINDLE
    PinBool spindle_force_active{"lcct.spindle-active"};
    PinDouble spindle_speed{"lcct.spindle-target-velocity"};

    // FAULT
    PinBool fault_reset{"lcct.fault-reset"};
};

#endif // MAINWINDOW_H
