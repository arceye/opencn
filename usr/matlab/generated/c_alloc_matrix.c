/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: c_alloc_matrix.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "c_alloc_matrix.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_emxutil.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : double M
 *                double N
 *                emxArray_real_T *A
 * Return Type  : void
 */
void c_alloc_matrix(double M, double N, emxArray_real_T *A)
{
    int loop_ub;
    int i;
    int b_loop_ub;
    int i1;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    loop_ub = (int)M;
    i = A->size[0] * A->size[1];
    A->size[0] = loop_ub;
    b_loop_ub = (int)N;
    A->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(A, i);
    for (i = 0; i < b_loop_ub; i++) {
        for (i1 = 0; i1 < loop_ub; i1++) {
            A->data[i1 + A->size[0] * i] = 0.0;
        }
    }
}

/*
 * File trailer for c_alloc_matrix.c
 *
 * [EOF]
 */
