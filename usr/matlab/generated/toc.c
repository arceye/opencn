/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: toc.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "toc.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include "timeKeeper.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : double
 */
double toc(void)
{
    double tstart_tv_sec;
    double tstart_tv_nsec;
    struct timespec b_timespec;
    b_timeKeeper(&tstart_tv_sec, &tstart_tv_nsec);
    clock_gettime(CLOCK_MONOTONIC, &b_timespec);
    return ((double)b_timespec.tv_sec - tstart_tv_sec) + ((double)
        b_timespec.tv_nsec - tstart_tv_nsec) / 1.0E+9;
}

/*
 * File trailer for toc.c
 *
 * [EOF]
 */
