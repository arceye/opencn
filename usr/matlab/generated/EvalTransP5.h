/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: EvalTransP5.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef EVALTRANSP5_H
#define EVALTRANSP5_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void EvalTransP5(double CurvStruct_CoeffP5[6][3], const double
                        u_vec_data[], const int u_vec_size[2], double r_0D_data[],
                        int r_0D_size[2], double r_1D_data[], int r_1D_size[2],
                        double r_2D_data[], int r_2D_size[2], double r_3D_data[],
                        int r_3D_size[2]);
extern void b_EvalTransP5(double CurvStruct_CoeffP5[6][3], double b_u_vec,
    double r_0D[3], double r_1D[3], double r_2D[3], double r_3D[3]);

#endif

/*
 * File trailer for EvalTransP5.h
 *
 * [EOF]
 */
