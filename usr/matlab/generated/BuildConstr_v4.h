/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: BuildConstr_v4.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef BUILDCONSTR_V4_H
#define BUILDCONSTR_V4_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void BuildConstr_v4(const CurvStruct CurvStructs_data[], const int
    CurvStructs_size[2], const double amax[3], double b_v_0, double b_at_0,
    double b_v_1, double b_at_1, const emxArray_real_T *b_BasisVal, const
    emxArray_real_T *b_BasisValD, const double u_vec_data[], const int
    u_vec_size[2], coder_internal_sparse *A, double b_data[], int b_size[1],
    double Aeq_data[], int Aeq_size[2], double beq_data[], int beq_size[1]);

#endif

/*
 * File trailer for BuildConstr_v4.h
 *
 * [EOF]
 */
