/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: sparse1.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef SPARSE1_H
#define SPARSE1_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void sparse_parenAssign(coder_internal_sparse *this, const
    emxArray_real_T *rhs, const double varargin_1_data[], const int
    varargin_1_size[2], const emxArray_real_T *varargin_2);
extern void sparse_spallocLike(int m, int n, int nzmax, emxArray_real_T *s_d,
    emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx, int *s_m, int *s_n,
    int *s_maxnz);

#endif

/*
 * File trailer for sparse1.h
 *
 * [EOF]
 */
