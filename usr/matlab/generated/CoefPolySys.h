/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CoefPolySys.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef COEFPOLYSYS_H
#define COEFPOLYSYS_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void CoefPolySys(const double in1[3], const double in2[3], const double
                        in3[3], double kappa0, const double in5[3], const double
                        in6[3], const double in7[3], double kappa1, double
                        CoefPS[16]);

#endif

/*
 * File trailer for CoefPolySys.h
 *
 * [EOF]
 */
