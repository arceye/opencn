/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: sparse.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "sparse.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : double varargin_1
 *                double varargin_2
 *                emxArray_real_T *y_d
 *                emxArray_int32_T *y_colidx
 *                emxArray_int32_T *y_rowidx
 *                int *y_m
 *                int *y_n
 *                int *y_maxnz
 * Return Type  : void
 */
void sparse(double varargin_1, double varargin_2, emxArray_real_T *y_d,
            emxArray_int32_T *y_colidx, emxArray_int32_T *y_rowidx, int *y_m,
            int *y_n, int *y_maxnz)
{
    int y_n_tmp;
    int i;
    int loop_ub;
    y_n_tmp = (int)varargin_2;
    i = y_d->size[0];
    y_d->size[0] = 1;
    emxEnsureCapacity_real_T(y_d, i);
    y_d->data[0] = 0.0;
    loop_ub = y_n_tmp + 1;
    i = y_colidx->size[0];
    y_colidx->size[0] = loop_ub;
    emxEnsureCapacity_int32_T(y_colidx, i);
    for (i = 0; i < loop_ub; i++) {
        y_colidx->data[i] = 1;
    }

    i = y_rowidx->size[0];
    y_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(y_rowidx, i);
    y_rowidx->data[0] = 1;
    *y_m = (int)varargin_1;
    *y_n = y_n_tmp;
    *y_maxnz = 1;
}

/*
 * File trailer for sparse.c
 *
 * [EOF]
 */
