/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: find.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "find.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : const boolean_T x_data[]
 *                const int x_size[1]
 *                int i_data[]
 *                int i_size[1]
 * Return Type  : void
 */
void eml_find(const boolean_T x_data[], const int x_size[1], int i_data[], int
              i_size[1])
{
    int nx;
    int idx;
    int ii;
    boolean_T exitg1;
    nx = x_size[0];
    idx = 0;
    i_size[0] = x_size[0];
    ii = 0;
    exitg1 = false;
    while ((!exitg1) && (ii <= nx - 1)) {
        if (x_data[ii]) {
            idx++;
            i_data[idx - 1] = ii + 1;
            if (idx >= nx) {
                exitg1 = true;
            } else {
                ii++;
            }
        } else {
            ii++;
        }
    }

    if (x_size[0] == 1) {
        if (idx == 0) {
            i_size[0] = 0;
        }
    } else if (1 > idx) {
        i_size[0] = 0;
    } else {
        i_size[0] = idx;
    }
}

/*
 * File trailer for find.c
 *
 * [EOF]
 */
