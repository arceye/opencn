/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: vertcat.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "vertcat.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include "sparse1.h"

/* Function Definitions */

/*
 * Arguments    : const coder_internal_sparse varargin_1
 *                const coder_internal_sparse varargin_2
 *                emxArray_real_T *c_d
 *                emxArray_int32_T *c_colidx
 *                emxArray_int32_T *c_rowidx
 *                int *c_m
 *                int *c_n
 * Return Type  : void
 */
void b_sparse_vertcat(const coder_internal_sparse varargin_1, const
                      coder_internal_sparse varargin_2, emxArray_real_T *c_d,
                      emxArray_int32_T *c_colidx, emxArray_int32_T *c_rowidx,
                      int *c_m, int *c_n)
{
    int cnfixeddim;
    boolean_T emptyflag_idx_0;
    boolean_T emptyflag_idx_1;
    boolean_T allEmpty;
    int cnnz;
    int cnvardim;
    int cidx;
    int kpstart;
    int kpend_tmp;
    int kpend;
    int kp;
    cnfixeddim = varargin_1.n;
    if ((varargin_1.m == 0) || (varargin_1.n == 0)) {
        emptyflag_idx_0 = true;
    } else {
        emptyflag_idx_0 = false;
    }

    if ((varargin_2.m == 0) || (varargin_2.n == 0)) {
        emptyflag_idx_1 = true;
    } else {
        emptyflag_idx_1 = false;
    }

    allEmpty = (emptyflag_idx_0 && emptyflag_idx_1);
    if ((!emptyflag_idx_1) && emptyflag_idx_0) {
        cnfixeddim = varargin_2.n;
    }

    cnnz = 0;
    cnvardim = 0;
    if (allEmpty || (!emptyflag_idx_0)) {
        cnnz = varargin_1.colidx->data[varargin_1.colidx->size[0] - 1] - 1;
        cnvardim = varargin_1.m;
    }

    if (allEmpty || (!emptyflag_idx_1)) {
        cnnz = (cnnz + varargin_2.colidx->data[varargin_2.colidx->size[0] - 1])
            - 1;
        cnvardim += varargin_2.m;
    }

    sparse_spallocLike(cnvardim, cnfixeddim, cnnz, c_d, c_colidx, c_rowidx, c_m,
                       c_n, &cidx);
    cnfixeddim = -1;
    if ((varargin_1.m == 0) || (varargin_1.n == 0)) {
        emptyflag_idx_0 = true;
    } else {
        emptyflag_idx_0 = false;
    }

    if ((varargin_2.m == 0) || (varargin_2.n == 0)) {
        emptyflag_idx_1 = true;
    } else {
        emptyflag_idx_1 = false;
    }

    for (cnnz = 0; cnnz < *c_n; cnnz++) {
        cnvardim = 0;
        if (!emptyflag_idx_0) {
            cidx = cnfixeddim;
            kpstart = varargin_1.colidx->data[cnnz];
            kpend_tmp = varargin_1.colidx->data[cnnz + 1];
            kpend = kpend_tmp - 1;
            for (kp = kpstart; kp <= kpend; kp++) {
                cidx++;
                c_rowidx->data[cidx] = varargin_1.rowidx->data[kp - 1];
                c_d->data[cidx] = varargin_1.d->data[kp - 1];
            }

            cnfixeddim = (cnfixeddim + kpend_tmp) - varargin_1.colidx->data[cnnz];
            cnvardim = varargin_1.m;
        }

        if (!emptyflag_idx_1) {
            cidx = cnfixeddim;
            kpstart = varargin_2.colidx->data[cnnz];
            kpend_tmp = varargin_2.colidx->data[cnnz + 1];
            kpend = kpend_tmp - 1;
            for (kp = kpstart; kp <= kpend; kp++) {
                cidx++;
                c_rowidx->data[cidx] = varargin_2.rowidx->data[kp - 1] +
                    cnvardim;
                c_d->data[cidx] = varargin_2.d->data[kp - 1];
            }

            cnfixeddim = (cnfixeddim + kpend_tmp) - varargin_2.colidx->data[cnnz];
        }

        c_colidx->data[cnnz + 1] = cnfixeddim + 2;
    }
}

/*
 * Arguments    : const emxArray_real_T *varargin_1_d
 *                const emxArray_int32_T *varargin_1_colidx
 *                const emxArray_int32_T *varargin_1_rowidx
 *                int varargin_1_m
 *                int varargin_1_n
 *                const double varargin_2_data[]
 *                const int varargin_2_size[2]
 *                emxArray_real_T *c_d
 *                emxArray_int32_T *c_colidx
 *                emxArray_int32_T *c_rowidx
 *                int *c_m
 *                int *c_n
 * Return Type  : void
 */
void sparse_vertcat(const emxArray_real_T *varargin_1_d, const emxArray_int32_T *
                    varargin_1_colidx, const emxArray_int32_T *varargin_1_rowidx,
                    int varargin_1_m, int varargin_1_n, const double
                    varargin_2_data[], const int varargin_2_size[2],
                    emxArray_real_T *c_d, emxArray_int32_T *c_colidx,
                    emxArray_int32_T *c_rowidx, int *c_m, int *c_n)
{
    int cnfixeddim;
    boolean_T emptyflag_idx_0;
    boolean_T emptyflag_idx_1;
    boolean_T allEmpty;
    int nzCount;
    int cnvardim;
    int kpstart;
    int crowoffs;
    int kpend;
    int cidx;
    double dk;
    cnfixeddim = varargin_1_n;
    if ((varargin_1_m == 0) || (varargin_1_n == 0)) {
        emptyflag_idx_0 = true;
    } else {
        emptyflag_idx_0 = false;
    }

    emptyflag_idx_1 = (varargin_2_size[1] == 0);
    allEmpty = (emptyflag_idx_0 && emptyflag_idx_1);
    if ((!emptyflag_idx_1) && emptyflag_idx_0) {
        cnfixeddim = varargin_2_size[1];
    }

    nzCount = 0;
    cnvardim = 0;
    if (allEmpty || (!emptyflag_idx_0)) {
        nzCount = varargin_1_colidx->data[varargin_1_colidx->size[0] - 1] - 1;
        cnvardim = varargin_1_m;
    }

    if (allEmpty || (varargin_2_size[1] != 0)) {
        kpstart = 0;
        crowoffs = varargin_2_size[0] * varargin_2_size[1];
        for (kpend = 0; kpend < crowoffs; kpend++) {
            if (varargin_2_data[kpend] != 0.0) {
                kpstart++;
            }
        }

        nzCount += kpstart;
        cnvardim += varargin_2_size[0];
    }

    sparse_spallocLike(cnvardim, cnfixeddim, nzCount, c_d, c_colidx, c_rowidx,
                       c_m, c_n, &kpstart);
    nzCount = -1;
    if ((varargin_1_m == 0) || (varargin_1_n == 0)) {
        emptyflag_idx_0 = true;
    } else {
        emptyflag_idx_0 = false;
    }

    emptyflag_idx_1 = (varargin_2_size[1] == 0);
    for (cnvardim = 0; cnvardim < *c_n; cnvardim++) {
        crowoffs = 1;
        if (!emptyflag_idx_0) {
            cidx = nzCount;
            kpstart = varargin_1_colidx->data[cnvardim];
            crowoffs = varargin_1_colidx->data[cnvardim + 1];
            kpend = crowoffs - 1;
            for (cnfixeddim = kpstart; cnfixeddim <= kpend; cnfixeddim++) {
                cidx++;
                c_rowidx->data[cidx] = varargin_1_rowidx->data[cnfixeddim - 1];
                c_d->data[cidx] = varargin_1_d->data[cnfixeddim - 1];
            }

            nzCount = (nzCount + crowoffs) - varargin_1_colidx->data[cnvardim];
            crowoffs = varargin_1_m + 1;
        }

        if (!emptyflag_idx_1) {
            kpend = varargin_2_size[0];
            cidx = nzCount;
            for (kpstart = 0; kpstart < kpend; kpstart++) {
                dk = varargin_2_data[kpstart + varargin_2_size[0] * cnvardim];
                if (dk != 0.0) {
                    cidx++;
                    c_rowidx->data[cidx] = kpstart + crowoffs;
                    c_d->data[cidx] = dk;
                }
            }

            nzCount = cidx;
        }

        c_colidx->data[cnvardim + 1] = nzCount + 2;
    }
}

/*
 * File trailer for vertcat.c
 *
 * [EOF]
 */
