/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedoptDefaultConfig.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "FeedoptDefaultConfig.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : FeedoptConfigStruct *cfg
 * Return Type  : void
 */
void FeedoptDefaultConfig(FeedoptConfigStruct *cfg)
{
    int i;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    cfg->NDiscr = 20;
    cfg->NBreak = 10;
    cfg->NHorz = 3;
    cfg->MaxNHorz = 10;
    cfg->MaxNDiscr = 200;
    cfg->MaxNCoeff = 120;
    cfg->vmax = 15.0;
    cfg->amax[0] = 20000.0;
    cfg->jmax[0] = 1.5E+6;
    cfg->amax[1] = 20000.0;
    cfg->jmax[1] = 1.5E+6;
    cfg->amax[2] = 20000.0;
    cfg->jmax[2] = 1.5E+6;
    cfg->SplineDegree = 4;
    cfg->CutOff = 0.1;
    cfg->LSplit = 2.0;
    cfg->v_0 = 0.1;
    cfg->at_0 = 0.0;
    cfg->v_1 = 0.1;
    cfg->at_1 = 0.0;
    for (i = 0; i < 1024; i++) {
        cfg->source[i] = ' ';
    }

    cfg->DebugPrint = false;

    /*      coder.varsize('cfg.source', [1024, 1], [0,1]); */
}

/*
 * File trailer for FeedoptDefaultConfig.c
 *
 * [EOF]
 */
