/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: SplitCurvStructs.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef SPLITCURVSTRUCTS_H
#define SPLITCURVSTRUCTS_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void SplitCurvStructs(int QueueIn_id, int QueueOut_id, double L_split);

#endif

/*
 * File trailer for SplitCurvStructs.h
 *
 * [EOF]
 */
