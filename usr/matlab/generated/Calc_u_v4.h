/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: Calc_u_v4.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef CALC_U_V4_H
#define CALC_U_V4_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void Calc_u_v4(const struct0_T *b_Bl, const double Coeff_data[], const
                      int Coeff_size[1], double dt, double u[10000], int *N);

#endif

/*
 * File trailer for Calc_u_v4.h
 *
 * [EOF]
 */
