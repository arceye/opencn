#define GSL_DISABLE_DEPRECATED

#include <stddef.h>
#ifdef __KERNEL__
#include <linux/slab.h>
#include <opencn/gsl/gsl_bspline.h>
#include <opencn/ctypes/strings.h>
#else /* __KERNEL__ */
#include <gsl/gsl_bspline.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#endif /* __KERNEL__ */

#include "c_spline.h"

void c_bspline_create(unsigned long *handle, double x0, double x1, size_t degree, size_t nbreak)
{
    size_t ncoeffs;
    int nderiv;
#ifdef __KERNEL__
    bspline_t *bs = (bspline_t *)kmalloc(sizeof(bspline_t), GFP_ATOMIC);
#else
    bspline_t *bs = (bspline_t *)malloc(sizeof(bspline_t));
#endif
    bs->ws = gsl_bspline_alloc(degree, nbreak);
    gsl_bspline_knots_uniform(x0, x1, bs->ws);

    ncoeffs = gsl_bspline_ncoeffs(bs->ws);
    nderiv = 2;

    // the number of coeffs will not change so we can allocate dB once and forget about it
    bs->dB = gsl_matrix_alloc(ncoeffs, nderiv + 1);

    *handle = (unsigned long)bs;
}

// void c_bspline_create(unsigned long *handle) { *handle = (unsigned long)malloc(1024); }

void c_bspline_destroy(const unsigned long *handle)
{
    bspline_t *bs = (bspline_t *)*handle;
    gsl_bspline_free((gsl_bspline_workspace *)bs->ws);
    gsl_matrix_free(bs->dB);
#ifdef __KERNEL__
    kfree(bs);
#else
    free(bs);
#endif
}

void c_bspline_base_eval(const unsigned long *handle, size_t N, const double *xvec,
                         double *BasisVal, double *BasisValD, double *BasisValDD,
                         double *BasisIntegr)
{
    size_t k, i;
    bspline_t *bs = (bspline_t *)*handle;
    const int nderiv = 2;
    const size_t ncoeffs = gsl_bspline_ncoeffs(bs->ws);
    /* for each sample */
    for (k = 0; k < N; k++) {
        /* for each basis function */
        gsl_bspline_deriv_eval(xvec[k], nderiv, bs->dB, bs->ws);
        for (i = 0; i < ncoeffs; i++) {
            BasisVal[i * N + k] = gsl_matrix_get(bs->dB, i, 0);
            BasisValD[i * N + k] = gsl_matrix_get(bs->dB, i, 1);
            BasisValDD[i * N + k] = gsl_matrix_get(bs->dB, i, 2);
        }
    }

    for (i = 0; i < ncoeffs; i++) {
        BasisIntegr[i] = 0.0;
        for (k = 0; k < N; k++) {
            BasisIntegr[i] += BasisVal[i * N + k];
        }
    }
}

void c_bspline_eval(const unsigned long *handle, const double *c, double x, double X[3])
{
    size_t k, i;
    bspline_t *bs = (bspline_t *)*handle;
    const int nderiv = 2;
    const size_t ncoeffs = gsl_bspline_ncoeffs(bs->ws);

#ifdef __KERNEL__
    if (x < 0.0) {
        opencn_cprintf(OPENCN_COLOR_BRED, "c_bspline_eval: x < 0\n");
        x = 0.0;
    }
    if (x > 1.0) {
        opencn_cprintf(OPENCN_COLOR_BRED, "c_bspline_eval: x > 1\n");
        x = 1.0;
    }
#else
    if (x < 0.0) {
        printf("c_bspline_eval: x < 0\n");
        x = 0.0;
    }
    if (x > 1.0) {
        printf("c_bspline_eval: x > 1\n");
        x = 1.0;
    }
#endif

    gsl_bspline_deriv_eval(x, nderiv, bs->dB, bs->ws);
    /* for each deriv degree */
    for (k = 0; k < nderiv + 1; k++) {
        X[k] = 0.0;
        /* for each coeff */
        for (i = 0; i < ncoeffs; i++) {
            X[k] += gsl_matrix_get(bs->dB, i, k) * c[i];
        }
    }
}

void c_bspline_eval_vec(const unsigned long *handle, const double *c, size_t N, double *xvec,
                        double X[][3])
{
    size_t i;
    /* for each sample */
    for (i = 0; i < N; i++) {
        c_bspline_eval(handle, c, xvec[i], (double *)&X[i]);
    }
}

gsl_vector *zeros(size_t n)
{
    gsl_vector *v = gsl_vector_alloc(n);
    gsl_vector_set_zero(v);
    return v;
}

size_t c_bspline_ncoeff(const unsigned long *handle)
{
    bspline_t *bs = (bspline_t *)*handle;
    return gsl_bspline_ncoeffs(bs->ws);
}
