function [C, success] = c_simplex(f, A, b, Aeq, beq)
    if coder.target('rtw') || coder.target('mex')
        c_prof_in(mfilename);
        c_prof_in('join_A');
        Atot = [A; Aeq];
        c_prof_out('join_A');
        MaxCoeff = FeedoptLimits.MaxNCoeff*FeedoptLimits.MaxNHorz;
        C = zeros(MaxCoeff, 1);
        coder.varsize('C', [MaxCoeff, 1], [1,0]);
        
        [Aisd, Ajsd, Avs] = find(Atot);
        Ais = int32(Aisd)-1;
        Ajs = int32(Ajsd)-1;
        % 25000 was estimated when running it with limit values
        MaxValues = FeedoptLimits.MaxNDiscr*FeedoptLimits.MaxNBreak*FeedoptLimits.MaxNHorz;
        coder.varsize('Avs', [MaxValues, 1], [1, 0]);
        coder.varsize('Ais', [MaxValues, 1], [1, 0]);
        coder.varsize('Ajs', [MaxValues, 1], [1, 0]);
        
        fsize = int32(size(f));
        Asize = int32(size(Atot));
        An = int32(nnz(Avs));
        bsize = int32(size(b));
        beqsize = int32(size(beq));
        Csize = int32(size(C));
        success = int32(0);
        success = coder.ceval('simplex_solve', coder.rref(f), fsize,... 
            coder.rref(Avs), coder.rref(Ais), coder.rref(Ajs), Asize, An,...
            coder.rref(b), bsize, coder.rref(beq),beqsize,...
            coder.ref(C), Csize);
%         C = solution.solution; 

        c_prof_out(mfilename);
    else
%         options = optimoptions('linprog', 'OptimalityTolerance', 1e-6, ...
%                        'ConstraintTolerance', 1e-6, 'Display', 'iter');
%         [C, ~, exitflag, output] = linprog(f, A, b, Aeq, beq, zeros(numel(f),1), [], options);
%         success = exitflag == 1;
%         if ~success
%             fprintf('linprog failed with exitflag = %d\n', exitflag);
%         end
        [C0, success] = c_simplex_mex(f, A, b, Aeq, beq);
        C = C0(1:numel(f));
    end
end
