function QueueResize(id, n, DefaultValue)
global g_QueueData
g_QueueData{id} = repmat(DefaultValue, 1, n);
end