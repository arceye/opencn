function [r0D, r1D, r2D, r3D] = EvalCurvStruct(CurvStruct, u_vec)

if any(u_vec > 1.0)
    fprintf('EvalCurvStruct: u_vec > 1\n');
    u_vec(u_vec > 1.0) = 1.0;
end

if any(u_vec < 0.0)
    fprintf('EvalCurvStruct: u_vec < 0\n');
    u_vec(u_vec < 0.0) = 0.0;
end

%
Type  = CurvStruct.Type;
%
N = numel(u_vec);
r0D = zeros(3, N);
r1D = zeros(3, N);
r2D = zeros(3, N);
r3D = zeros(3, N);

switch Type
    case CurveType.Line % line (G01)
        [r0D, r1D, r2D, r3D] = EvalLine(CurvStruct, u_vec);
    case CurveType.Helix % arc of circle / helix (G02, G03)
        [r0D, r1D, r2D, r3D] = EvalHelix(CurvStruct, u_vec);
    case CurveType.TransP5 % polynomial transition
        [r0D, r1D, r2D, r3D] = EvalTransP5(CurvStruct, u_vec);
    otherwise
        c_assert(0, 'Unknown Curve Type for Eval.\n');
end
