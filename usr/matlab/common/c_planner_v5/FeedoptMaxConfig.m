function cfg = FeedoptMaxConfig()
cfg = struct(...
		'NDiscr', FeedoptLimits.MaxNDiscr,...
		'NBreak', FeedoptLimits.MaxNBreak,...
		'NHorz', FeedoptLimits.MaxNHorz,...
        'MaxNHorz', FeedoptLimits.MaxNHorz,...
        'MaxNDiscr', FeedoptLimits.MaxNDiscr,...
        'MaxNCoeff', FeedoptLimits.MaxNCoeff,...
		'vmax', 15,...
		'amax', [20000,20000,20000],...
		'jmax', [1500000,1500000,1500000],...
		'SplineDegree', int32(3),...
        'CutOff', 0.1,...
        'LSplit', 0.5,...
        'v_0', 0.1, 'at_0', 0,...
        'v_1', 0.1, 'at_1', 0);
end