function [L, R] = LengthHelix(CurvStruct)
%
P0      = CurvStruct.P0;
P1      = CurvStruct.P1;
evec    = CurvStruct.evec;
theta   = CurvStruct.theta;
pitch   = CurvStruct.pitch;
%
P0P1    = P1 - P0;
EcrP0P1 = cross(evec, P0P1);
%
P1proj  = P1 - (P0P1'*evec)*evec;
C       = ((P0 + P1proj) + cot(theta/2)*EcrP0P1) / 2;
CP0     = P0 - C;
%
R       = MyNorm(CP0);
L       = theta*sqrt(R^2 + (pitch/(2*pi))^2);





