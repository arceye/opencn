function cfg = FeedoptDefaultConfig()
cfg = struct(...
		'NDiscr', int32(20),...
		'NBreak', int32(10),...
		'NHorz', int32(3),...
        'MaxNHorz', FeedoptLimits.MaxNHorz,...
        'MaxNDiscr', FeedoptLimits.MaxNDiscr,...
        'MaxNCoeff', FeedoptLimits.MaxNCoeff,...
		'vmax', 15,...
		'amax', [20000,20000,20000],...
		'jmax', [1500000,1500000,1500000],...
		'SplineDegree', int32(4),...
        'CutOff', 0.1,...
        'LSplit',2.0,...
        'v_0', 0.1, 'at_0', 0,...
        'v_1', 0.1, 'at_1', 0,...
        'source', repmat(' ', [1024, 1]), ...
        'DebugPrint', false);
%     coder.varsize('cfg.source', [1024, 1], [0,1]);
end