function PlotCurvStructs(CurvStructs)
figure
u = linspace(0,1,50);
args = {};
for k = 1:length(CurvStructs)
    P = EvalCurvStruct(CurvStructs(k), u);
    args{end+1} = P(1, :);
    args{end+1} = P(2, :);
    args{end+1} = P(3, :);
end
args
plot3(args{:})
legend
end