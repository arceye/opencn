function [u, pop_next, finished] = ResampleTick(CurOptStruct, NextOptStruct, Bl, u, dt)

if CurOptStruct.CurvStruct.Type == CurveType.None
   u = 0;
   pop_next = false;
   finished = true;
   return;
end

finished = false;

uk = u;

[qk, dk, ~] = bspline_eval(Bl, CurOptStruct.Coeff, uk);
ukp1 = uk + dk*dt^2/4 + sqrt(qk)*dt;

if ukp1 < uk
    fprintf('!!! ukp1 < uk !!!\n');
    finished = true;
    pop_next = false;
    u = 0;
    return;
end

%
if ukp1 < 1
    uk   = ukp1;
    u = uk;
    pop_next = false;
else
    ukp1      = 1;
    [qkp1,~] = bspline_eval(Bl, CurOptStruct.Coeff, ukp1);
    Trest     = 2*(ukp1 - uk) / (sqrt(qkp1) + sqrt(qk));
    if Trest > dt
        Trest = dt;
        fprintf('!!! Trest > dt !!!\n')
    end
    %
    dt_begin  = dt - Trest;
    uk        = 0;
    
    if NextOptStruct.CurvStruct.Type ~= CurveType.None
        [qk, dk] = bspline_eval(Bl, NextOptStruct.Coeff, uk);
        ukp1      = uk + dk*dt_begin^2/4 + sqrt(qk)*dt_begin; 
        if ukp1 < uk
            fprintf('!!! ukp1 < uk !!!\n');
            finished = true;
            pop_next = false;
            u = 0;
            return;
        end
        u = ukp1;
        pop_next = true;
    else
        finished = true;
        pop_next = false;
        u = 1;
    end
end
