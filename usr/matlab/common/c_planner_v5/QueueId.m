classdef QueueId < int32
    enumeration
        GCode(0)
        Smooth(1)
        Split(2)
        Opt(3)
        COUNT(4)
    end
    methods(Static)
        function value = addClassNameToEnumNames()
            value = true;
        end
    end
end