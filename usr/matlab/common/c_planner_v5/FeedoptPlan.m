function op = FeedoptPlan(op)
global g_FeedoptConfig
persistent QueueGCode QueueSmooth QueueSplit QueueOpt
persistent Noptimized BasisVal BasisValD BasisValDD u_vec k0 Coeff NCoeff ReachedEnd
persistent v_0 v_1 at_0 at_1 Bl BasisIntegr
persistent TryAgain

if isempty(QueueGCode)
    QueueGCode = Queue(QueueId.GCode);
    QueueSmooth = Queue(QueueId.Smooth);
    QueueSplit = Queue(QueueId.Split);
    QueueOpt = Queue(QueueId.Opt);
end

if isempty(Bl)
    Bl = bspline_create(0, 1, g_FeedoptConfig.SplineDegree, g_FeedoptConfig.NBreak);
    Noptimized = int32(0);
    u_vec  = sinspace(0, 1, g_FeedoptConfig.NDiscr);
    coder.varsize('u_vec', [1, FeedoptLimits.MaxNDiscr], [0, 1]);
    [BasisVal, BasisValD, BasisValDD, BasisIntegr] = bspline_base_eval(Bl, u_vec);    
    Coeff = zeros(g_FeedoptConfig.MaxNCoeff, g_FeedoptConfig.MaxNHorz);
    k0 = int32(1);
    v_0 = g_FeedoptConfig.v_0;
    at_0 = g_FeedoptConfig.at_0;
    v_1 = g_FeedoptConfig.v_1;
    at_1 = g_FeedoptConfig.at_1;
    ReachedEnd = false;
    NCoeff = int32(0);
    TryAgain = false;
    fprintf('FEEDOPT_PLAN INITIAL RUN\n')
end


switch op
    case Fopt.Init
        QueueGCode = Queue(QueueId.GCode);
        QueueSmooth = Queue(QueueId.Smooth);
        QueueSplit = Queue(QueueId.Split);
        QueueOpt = Queue(QueueId.Opt);
        
        QueueGCode.Clear();
        QueueSmooth.Clear();
        QueueSplit.Clear();
        QueueOpt.Clear();
        op = Fopt.GCode;
        
        Noptimized = int32(0);
        k0 = int32(1);
        v_0 = g_FeedoptConfig.v_0;
        at_0 = g_FeedoptConfig.at_0;
        v_1 = g_FeedoptConfig.v_1;
        at_1 = g_FeedoptConfig.at_1;
        u_vec  = sinspace(0, 1, g_FeedoptConfig.NDiscr);
        bspline_destroy(Bl);
        Bl = bspline_create(0, 1, g_FeedoptConfig.SplineDegree, g_FeedoptConfig.NBreak);
        [BasisVal, BasisValD, BasisValDD, BasisIntegr] = bspline_base_eval(Bl, u_vec);
        
        Coeff = zeros(g_FeedoptConfig.MaxNCoeff, g_FeedoptConfig.MaxNHorz);
        TryAgain = false;
        
        coder.varsize('OptSegment', [1, FeedoptLimits.MaxNHorz], [0, 0]);
        coder.varsize('BasisVal', [FeedoptLimits.MaxNDiscr, FeedoptLimits.MaxNCoeff], [1, 1]);
        coder.varsize('BasisValD', [FeedoptLimits.MaxNDiscr, FeedoptLimits.MaxNCoeff], [1, 1]);
        coder.varsize('BasisValDD', [FeedoptLimits.MaxNDiscr, FeedoptLimits.MaxNCoeff], [1, 1]);
        coder.varsize('BasisIntegr', [FeedoptLimits.MaxNCoeff, 1], [1 0]);
        
        coder.varsize('Coeff', [FeedoptLimits.MaxNCoeff, FeedoptLimits.MaxNHorz], [1,1]);
        
        fprintf('Size of u_vec: [%d %d]\n', int32(size(u_vec,1)), int32(size(u_vec,2)));
        fprintf('Size of BasisIntegr: [%d %d]\n', int32(size(BasisIntegr,1)), int32(size(BasisIntegr,2)));
        
        ReachedEnd = false;
        NCoeff = int32(0);
        
        fprintf('Starting optimization with NHorz = %d\n', g_FeedoptConfig.NHorz);
        
    case Fopt.GCode
        QueueGCode.Clear();
        QueueSmooth.Clear();
        QueueSplit.Clear();
        QueueOpt.Clear();
        status = int32(1);
        while status
            status = ReadGCode(QueueGCode);
        end
        op = Fopt.Smooth;
    case Fopt.Smooth
        SmoothCurvStructs(QueueGCode, QueueSmooth, g_FeedoptConfig.CutOff);
        op = Fopt.Split;
    case Fopt.Split
        SplitCurvStructs(QueueSmooth, QueueSplit, g_FeedoptConfig.LSplit);
        op = Fopt.Opt;
    case Fopt.Opt
        if QueueSplit.Size == 0
            op = Fopt.Finished;
            return;
        end
        op = Fopt.Opt;
       
        
%         C1 = QueueSplit.Get(1);
%         C1.FeedRate = 10;
%         QueueSplit.Set(1, C1);
        
        if Noptimized < QueueSplit.Size
            % @TODO: Handle NN, NZ ... here
            k1temp = k0 + g_FeedoptConfig.NHorz - 1;
            if k1temp > QueueSplit.Size
                ReachedEnd = true;
                k1 = QueueSplit.Size;
            else
                k1 = k1temp;
            end
            
%             fprintf('Opt %d, k0 = %2d, k1 = %2d\n', Noptimized + 1, k0, k1);
            
            if TryAgain
                % Do nothing, we already have the last one optimized
            elseif ~ReachedEnd
                OptSegment = repmat(QueueSplit.ValueType, 1, FeedoptLimits.MaxNHorz);
                
                DebugLog('============= FEEDRATE PLANNING ================\n')
                for k = k0:k1
                    OptSegment(k-k0+1) = QueueSplit.Get(k);
                    PrintCurvStruct(OptSegment(k-k0+1))
                    if k < k1
                        DebugLog('-----------------------------------\n')
                    end
                end
                DebugLog('================================================\n')
                
                [Coeff, NCoeff, v_0, at_0, success] = FeedratePlanning_v4(OptSegment, g_FeedoptConfig.amax, g_FeedoptConfig.jmax,...
                    v_0, at_0, v_1, at_1, BasisVal, BasisValD, BasisValDD, BasisIntegr,...
                    Bl, u_vec, g_FeedoptConfig.NHorz);
                if success == 0
                    fprintf('OPTIMIZATION FAILED!\n');
                    op = Fopt.Finished;
                    return;
                end
            else
                % If we have reached the end of the optimizing segment, we
                % can just copy out the coefficients for the whole horizon
                Coeff(:, 1:end-1) = Coeff(:, 2:end);
            end
           
            
            if coder.target('rtw')
                push_status = PushStatus.Success;
                push_status = coder.ceval('feedopt_push_opt', QueueSplit.Get(Noptimized + 1), Coeff(:, 1), NCoeff);
                switch push_status
                    case PushStatus.Success
                        TryAgain = false;
                        k0 = k0 + 1;
                        Noptimized = Noptimized + 1;
                    case PushStatus.TryAgain
                        TryAgain = true;
                    case PushStatus.Finished
                        TryAgain = false;
                        op = Fopt.Finished;
                end
            else
%                 Coeff(1:NCoeff, 1)'
                QueueOpt.Push(struct('Coeff', Coeff(1:NCoeff, 1), 'CurvStruct', QueueSplit.Get(Noptimized + 1)));
                k0 = k0 + 1;
                Noptimized = Noptimized + 1;
            end
            
            
        else
            op = Fopt.Finished;
        end
    case Fopt.Finished
        op = Fopt.Finished;
    otherwise
        fprintf('FEEDOPT: WRONG STATE\n')
        op = Fopt.Finished;
end
end