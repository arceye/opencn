function Test_FeedratePlanning_v4_c
%
if ~coder.target('rtw')
    clc; clear all; close all;
end
%
% meco_binaries('cpp_splines','develop')
% import splines.*
%
%% kinematic constraints
vmax   = 1.5;              % max feedrate in [m/s]
amax   = 4.5*ones(3, 1);   % max acceleration per axis [m/s^2]
jmax   = 100*ones(3, 1);   % max jerk per axis [m/s^3]

global g_FeedoptConfig
InitConfig();
QueueInit();

g_FeedoptConfig.DebugPrint = false;

% reshape([0.15 -0.05 0.00 -0.37 0.10 0.00 0.00 -0.04 0.00 0.42 0.00 0.00 0.00 0.00 0.00 0.90 0.00 0.00], 3, []) 
%
%% plot curve pieces
% if ~coder.target('rtw')
%     PlotCurvStructs_v4(CurvStructs0, CurvStructs0);
% end
% return
%% boundary conditions
v_0    =  0.1;
at_0   =  0.0;
v_1    =  0.1;
at_1   =  0.0;

g_FeedoptConfig.NDiscr = 200;
g_FeedoptConfig.NBreak = 100;
g_FeedoptConfig.NHorz = 10;

%
%% generate BSpline basis functions and their derivatives evaluated on a grid
u_vec  = sinspace(0, 1, g_FeedoptConfig.NDiscr);
Bl = bspline_create(0, 1, g_FeedoptConfig.SplineDegree, g_FeedoptConfig.NBreak);
% [BasisVal, BasisValD, BasisValDD, BasisIntegr] = bspline_base_eval(Bl, u_vec);
% 
% QueueGCode = Queue(QueueId.GCode);
% QueueSmooth = Queue(QueueId.Smooth);
% QueueSplit = Queue(QueueId.Split);
QueueOpt = Queue(QueueId.Opt);

op = Fopt.Init;

while op ~= Fopt.Finished
    g_FeedoptConfig.DebugPrint = true;
    op = FeedoptPlan(op);
end


% CurvGCode = QueueGCode.Values;
CurvOpt = QueueOpt.Values;
% PlotCurvStructs_v4(CurvOpt, CurvOpt);

% load pieces_demo/PieceDemoLinuxCNC_HV_V04_CS_Opt.mat CurvOpt

% 

CurOptStruct = CurvOpt(1);
NextOptStruct = CurvOpt(2);
N = 1;
u = 0;
uvec = zeros(20000, 1);
pvec = zeros(20000, 3);
dt = 1e-3;
for ktick = 1:length(uvec)
    r0D = EvalCurvStruct(CurOptStruct.CurvStruct, u);
    pvec(ktick, :) = r0D;
    [u, pop_next, finished] = ResampleTick(CurOptStruct, NextOptStruct, Bl, u, dt);
    
    uvec(ktick) = u;
    
    if pop_next
        N = N + 1;
        CurOptStruct = CurvOpt(N);
        if N < numel(CurvOpt)
            NextOptStruct = CurvOpt(N+1);
        else
            NextOptStruct.CurvStruct.Type = CurveType.None;
        end
    end
    
    if finished
        break;
    end
end

% uvec = uvec(1:ktick, :);
pvec = pvec(1:ktick, :);

% figure
% plot(uvec);
% ylim([-0.1, 1.1])

v = [[0,0,0]; diff(pvec, 1)];
v = vecnorm(v, 2, 2)/dt;

figure

scatter3(pvec(:, 1), pvec(:, 2), pvec(:, 3), 1, v, '.')
colormap jet
set(gca, 'Projection','perspective')
axis vis3d
% equal
colorbar

figure
plot(v)

% while 1
%     FeedoptPlan("plan");
% %     CurvStructs0 = g_QueueGCode.Values(1:g_QueueGCode.Size);
% %     CurvStructs1 = g_QueueOpt.Values(1:g_QueueOpt.Size);
%     PlotCurvStructs_v4(CurvStructs1, CurvStructs1);
%     if g_QueueOpt.Size >= g_FeedoptConfig.NHorz
%         [Coeff, v_0, at_0] = FeedratePlanning_v4(CurvStructs1, g_FeedoptConfig.amax, g_FeedoptConfig.jmax, v_0, at_0, v_1, at_1, ...
%                             BasisVal, BasisValD, BasisValDD, BasisIntegr, Bl, u_vec, g_FeedoptConfig.NHorz);  
%     end
%       
% %     Coeff
%     PopQueueOpt();
%     pause
% end

%

return


if ~coder.target('rtw')
    %% resampling
    dt     = 1e-3;
    u_vecr = u_vec;
    %
    [u_cell, t] = Calc_u_v4(Bl, Coeff, dt);
    T           = t(end);  % final time
    %
    [v_norm, a, j] = CalcVAJ_v5(CurvStructs, Bl, Coeff, u_cell);
    %
    %


    figure;
    plot(t, v_norm, [0, T], [vmax, vmax], 'r--');
    grid;
    axis('tight');
    title('optimal ||v(t)||');
    xlabel('t');
    ylabel('v [m/s]');
    ylim([0, max([1.1*vmax, max(max(v_norm))])]);

    figure;
    plot(t, a(1, :), 'b', t, a(2, :), 'g', t, a(3, :), 'r', ...
         [0, t(end)], [amax, amax], 'r--', [0, t(end)], [-amax, -amax], 'r--');
    grid;
    legend('a_x(t)', 'a_y(t)', 'a_z(t)');
    axis('tight');
    title('optimal a(t)');
    xlabel('t');
    ylabel('a [m/s^2]');
    ylim([min([-1.1*amax(1), min(min(a))]), max([1.1*amax(1), max(max(a))])]);
    %% active constraints 
    figure;
    plot(t, j(1, :), 'b', t, j(2, :), 'g', t, j(3, :), 'r', ...
         [0, t(end)], [jmax, jmax], 'r--', [0, t(end)], [-jmax, -jmax], 'r--');
    grid;
    axis('tight');
    title('optimal j(t)');
    xlabel('t');
    ylabel('j [m/s^3]');
    legend('j_x(t)', 'j_y(t)', 'j_z(t)');
    ylim([min([-1.1*jmax(1), min(min(j))]), max([1.1*jmax(1), max(max(j))])]);
    %
    figure;
    plot(t, v_norm/vmax, 'b', ...
         t, abs(a(1, :))/amax(1), 'g', t, abs(a(2, :))/amax(2), 'g', t, abs(a(3, :))/amax(3), 'g', ...
         t, abs(j(1, :))/jmax(1), 'r', t, abs(j(2, :))/jmax(2), 'r', t, abs(j(3, :))/jmax(3), 'r');
    xlabel('t');
    title('active constraints : v (blue), a (green), j (red)');
    ylim([0, 1.1]);
end
