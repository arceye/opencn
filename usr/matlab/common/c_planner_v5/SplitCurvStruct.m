function SplitCurvStruct(CrvStrct, QueueOut, L_split)
%
Type  = CrvStrct.Type;

%
switch Type
    case CurveType.Line % line (G01)
        if LengthCurv(CrvStrct) > L_split*2
            SplitLine(CrvStrct, QueueOut, L_split);
        else
            QueueOut.Push(CrvStrct);
        end
    case CurveType.Helix % arc of circle / helix (G02, G03)
        if LengthCurv(CrvStrct) > L_split*2
            SplitHelix(CrvStrct, QueueOut, L_split);
        else
            QueueOut.Push(CrvStrct);
        end
    otherwise
        QueueOut.Push(CrvStrct); % CrvStrctSplit = CrvStrct;
end
