function TestArcs

S1 = ConstrHelixStruct([0,0,0]', [-1,1,0]', [0,0,-1]', pi/2, 0, 1, ZSpdMode.NN);
S2 = ConstrHelixStruct([-1,1,0]', [-2,0,0]', [0,0,1]', 3*pi/2, 0, 1, ZSpdMode.NN);

CurvStructs = [S1, S2];
save('pieces_demo/ArcTest.mat', 'CurvStructs');

end

