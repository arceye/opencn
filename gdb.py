import gdb
from subprocess import check_output, CalledProcessError, Popen, PIPE

class AttachPidofCommand (gdb.Command):
  "Attach to process by name"

  def __init__ (self):
    super (AttachPidofCommand, self).__init__ ("attach_pidof",
                         gdb.COMMAND_SUPPORT,
                         gdb.COMPLETE_NONE, True)

  def invoke (self, arg, from_tty):
    try:
        pid = check_output(["pidof", arg]).split()[0].decode("utf-8")
    except CalledProcessError:
        gdb.write('process \'%s\' not found\n' % (arg))
        return
    gdb.write('attach to \'%s\' (%s)\n' % (arg, pid))
    gdb.execute('attach %s' % (pid), from_tty)

AttachPidofCommand()

class AddSymbolFileTextCommand (gdb.Command):
  "Like add-symbol-file but with automatic .text address using readelf"

  def __init__ (self):
    super (AddSymbolFileTextCommand, self).__init__ ("add_symbol_file_text",
                         gdb.COMMAND_SUPPORT,
                         gdb.COMPLETE_NONE, True)

  def invoke (self, arg, from_tty):
    try:
        out = check_output(["readelf", "-WS", arg]).decode("utf-8")
        lines_with_text = list(filter(lambda x: ".text" in x, out.split('\n')))
        if len(lines_with_text) == 0:
          gdb.write('file \'%s\' has not .text section\n' % (arg))
          return
        
        addr = lines_with_text[0].split()[4]
    except CalledProcessError:
        gdb.write('error: %s\n' % (out))
        return
    gdb.write('add-symbol-file \'%s\' .text: 0x%s\n' % (arg, addr))
    gdb.execute('add-symbol-file %s 0x%s' % (arg, addr), from_tty)

AddSymbolFileTextCommand()