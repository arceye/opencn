#ifndef __GSL_VERSION_H__
#define __GSL_VERSION_H__

#include <opencn/gsl/gsl_types.h>


#define GSL_VERSION "2.5.opencn"
#define GSL_MAJOR_VERSION 2
#define GSL_MINOR_VERSION 5

GSL_VAR const char * gsl_version;


#endif /* __GSL_VERSION_H__ */
