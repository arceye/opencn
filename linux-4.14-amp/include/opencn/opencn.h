
#ifndef OPENCN_H
#define OPENCN_H

#include <linux/spinlock.h>
#include <linux/percpu.h>
#include <linux/smp.h>
#include <linux/jiffies.h>

#include <asm/atomic.h>

#ifndef RTDM_HZ
#define RTDM_HZ     CONFIG_RTDM_HZ
#endif /* RTDM_HZ */

extern unsigned long volatile __cacheline_aligned_in_smp __jiffy_arch_data rtdm_jiffies;

#define MAX_CPU_DOMAINS		2
#define MAX_DOMAINS		MAX_CPU_DOMAINS

/* DOMID_SELF is used in certain contexts to refer to oneself. */
#define DOMID_SELF 		0x7FF0U

/* Agency */
#define DOMID_CPU0		0

/* Realtime CPU domain */
#define DOMID_RT_CPU		1

#define NSECS           	1000000000ull
#define SECONDS(_s)     	((u64)((_s)  * 1000000000ull))
#define MILLISECS(_ms)  	((u64)((_ms) * 1000000ull))
#define MICROSECS(_us)  	((u64)((_us) * 1000ull))

#define VBUS_TASK_PRIO		50

/*
 * The priority of the Directcomm thread must be higher than the priority of the SDIO
 * thread to make the Directcomm thread process a DC event and release it before any new
 * request by the SDIO's side thus avoiding a deadlock.
 */
#define DC_ISR_TASK_PRIO	55

/*
 * non-RT CPU: OPENCN_CPU0, OPENCN_CPU03
 * RT CPU: OPENCN_RT_CPU
 */
#define OPENCN_CPU0	        0
#define OPENCN_RT_CPU	 	1
#define OPENCN_CPU2		2
#define OPENCN_CPU3		3

#ifndef __ASSEMBLY__

/*
 * 1024 event channels per domain
 */
#define NR_EVTCHN 1024

extern volatile bool __xenomai_ready_to_go;
extern volatile bool __cobalt_ready;

typedef uint16_t domid_t;

struct shared_info {
	uint8_t evtchn_upcall_pending;

    /*
     * A domain can create "event channels" on which it can send and receive
     * asynchronous event notifications. There are three classes of event that
     * are delivered by this mechanism:
     *  1. Bi-directional inter- and intra-domain connections. Domains must
     *     arrange out-of-band to set up a connection (usually by allocating
     *     an unbound 'listener' port and avertising that via a storage service
     *     such as vbstore).
     *  2. Physical interrupts. A domain with suitable hardware-access
     *     privileges can bind an event-channel port to a physical interrupt
     *     source.
     *  3. Virtual interrupts ('events'). A domain can bind an event-channel
     *     port to a virtual interrupt source, such as the virtual-timer
     *     device or the emergency console.
     *
     * To expedite scanning of pending notifications, any 0->1 pending
     * transition on an unmasked channel causes a corresponding bit in a
     * per-vcpu selector word to be set. Each bit in the selector covers a
     * 'C long' in the PENDING bitfield array.
     */
    unsigned long evtchn_pending[NR_EVTCHN/32];

    atomic_t dc_event;

};

typedef struct shared_info shared_info_t;

struct dom_evtchn;

struct evtchn
{
	u8  state;             /* ECS_* */

	bool can_notify;

	struct {
		domid_t remote_domid;
	} unbound;     /* state == ECS_UNBOUND */

	struct {
		u16            remote_evtchn;
		struct domain *remote_dom;
	} interdomain; /* state == ECS_INTERDOMAIN */

	u16 virq;      /* state == ECS_VIRQ */

};

struct domain {
	domid_t          domain_id;

	spinlock_t       event_lock;
	struct evtchn    evtchn[NR_EVTCHN];
	int		 processor;

	shared_info_t   *shared_info;
};

DECLARE_PER_CPU(struct domain, domain);

#define cpu_shared_info (per_cpu(domain, smp_processor_id()).shared_info)
#define current_domID() (per_cpu(domain, smp_processor_id()).domain_id)

#endif /* !__ASSEMBLY__ */

#endif /* OPENCN_H */

