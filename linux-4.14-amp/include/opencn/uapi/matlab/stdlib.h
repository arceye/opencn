#ifndef STDLIB_H
#define STDLIB_H

#ifdef __KERNEL__

#include <linux/types.h>

void* calloc(size_t nmemb, size_t size);
void* malloc(size_t size);
void free(void* ptr);
#endif

#endif /* STDLIB_H */
