/*
 * Copyright (C) 2019 Kevin Joly <kevin.joly@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <xenomai/rtdm/driver.h>

#define HALF_PERIOD_NS (100000)
#define PRIORITY (50)

/* 8250 */
#define REG8250_IO_OFFSET (0x3F8)

#define REG8250_MCR_OFFSET (REG8250_IO_OFFSET + 4)

#define REG8250_MCR_RTS (0x02)

static rtdm_task_t task_handle;

static void set_pin(void)
{
	outb(REG8250_MCR_RTS, REG8250_MCR_OFFSET);
}

static void reset_pin(void)
{
	outb(0, REG8250_MCR_OFFSET);
}

static void rts_toggle_task(void* arg)
{
	int ret;

	while (true) {
		set_pin();
		ret = rtdm_task_wait_period(NULL);
		if (ret != 0) {
			printk(KERN_ERR "rtdm_task_wait_period returned %d\n", ret);
            break;
        }

		reset_pin();
		ret = rtdm_task_wait_period(NULL);
		if (ret != 0) {
			printk(KERN_ERR "rtdm_task_wait_period returned %d\n", ret);
            break;
        }
	}
}

static int __init rts_toggle_init(void)
{
	int ret;

	ret = rtdm_task_init(&task_handle, "rts_toggle", rts_toggle_task, NULL,
                         PRIORITY, HALF_PERIOD_NS);

	return ret;
}

static void __exit rts_toggle_cleanup(void)
{
	rtdm_task_destroy(&task_handle);
}

module_init(rts_toggle_init);
module_exit(rts_toggle_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Kevin JOLY");
MODULE_DESCRIPTION("A simple module toggling RTS pin in RTDM domain");
