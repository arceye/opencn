/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@soo.tech>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#if 0
#define DEBUG
#endif

#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/completion.h>
#include <linux/irqreturn.h>

#include <linux/mmc/core.h>

#include <asm/fpu/api.h>

#include <xenomai/rtdm/driver.h>

#include <opencn/frontend/vdummy.h>

#include <opencn/ctypes/strings.h>

#include <opencn/event/evtchn-mgr.h>

#include <opencn/hal/hal.h>
#include <opencn/strtox.h>
#include <opencn/rtapi/rtapi_math.h>
#include <opencn/uapi/hal.h>

#include <opencn/gsl/gsl_bspline.h>

/* This code is mainly for debugging purposes. */

#define PERIOD_US (100)
#define PERIOD_NS (PERIOD_US*1000)

rtdm_task_t rt_task1;
rtdm_task_t rt_task2;
rtdm_task_t rt_task3;

rtdm_event_t rt_event1;

//rtdm_timer_t rt_timer;

void do_enable_rt(dc_event_t dc_event) {
	lprintk("## Processing DC_ENABLE_RT...\n");

}

static void rt_task3_fn(void *args) {
	static int i = 0;
	lprintk("### NOW waiting on rt_event1\n");

	while (1) {
		rtdm_event_wait(&rt_event1);

		lprintk("## T3 event got\n");

	}

}
static void rt_task2_fn(void *args) {
	static int i = 0;

	while (!rtdm_task_should_stop()) {

		lprintk("## T2 i: %d\n", i++);
		if (i == 15)
			return;

		rtdm_task_wait_period(NULL);

	}
	printk("### T2 ending...\n");
}

#define __PERIOD_NS MICROSECS(500)

static void beckhoff_task_fn(void *args) {
	uint64_t now, after;
	uint32_t last_app_time = 0u;
	uint64_t app_time = 0ull;
	uint64_t app_time_base;
	uint64_t nextstart;
	int ret;

	app_time_base = rtdm_clock_read_monotonic();
	nextstart = app_time_base;

	while (true) {

#if 0
		now = rtdm_clock_read_monotonic();

		rtdm_task_sleep(PERIOD_NS);

		after = rtdm_clock_read_monotonic();
		printk("D: %lu\n", after-now);

		if (nextstart > now) {
			ret = rtdm_task_sleep_abs(nextstart, RTDM_TIMERMODE_ABSOLUTE);
			if (ret) {
				lprintk(KERN_ERR "rtdm_task_sleep_until: %d\n", ret);
				break;
			}
		}
		now = rtdm_clock_read_monotonic();


		if (now - nextstart >__PERIOD_NS)
			printk("D = %lu\n", now-nextstart);

		/* receive process data */
		nextstart += __PERIOD_NS;
		//printk("######## nextstart = %lu\n", nextstart);
#endif
	}
}

static void rt_task1_fn(void *args) {

	nanosecs_abs_t time;
	int iter = 0;
	int ret;

#if 1

	//rtdm_task_init(&rt_task2, "rt_task_2", rt_task2_fn, NULL, 50, MILLISECS(500));

	//rtdm_task_destroy(&rt_task2);

	//lprintk("############# Okay destroyed\n");
	//rtdm_task_init(&rt_task3, "rt_task_3", rt_task3_fn, NULL, 50, 0);

	while (1) {
#if 0
		time = rtdm_clock_read_monotonic();
		lprintk("## TIME: %llu\n", time);
#endif

		lprintk("####### iter: %d\n", iter++);
		rtapi_print_msg(0, "Salut %d\n", iter);
		//rtdm_task_sleep(SECONDS(5));

		//rtdm_event_signal(&rt_event1);

		ret = rtdm_task_wait_period(NULL);
		if (ret < 0)
			printk("### ret = %d\n", ret);

        iter++;
        if (iter % 10000 == 0)
            printk("## good %d\n", iter);
	}
#endif

#if 0
	int i = 0;

	lprintk("## Registering callback for DC_ENABLE_RT dc_event...\n");
	rtdm_register_dc_event_callback(DC_ENABLE_RT, do_enable_rt);

#if 0
	lprintk("## Now initializing vdummy ring ...\n");
	vdummy_ring_init();

	vdummy_send_data("Hello salut !!\n");
#endif
	while (1) {

		lprintk("## i: %d\n", i++);

		rtdm_task_wait_period(NULL);

	}
#endif

}

#if 0
double try(double d) {

	return d*2.2;

}
#endif

void rtapi_task(void *args) {

	lprintk("############ HELLO from rtapi... \n");

}

/* Debugging the hal/rtapi framework */
#if 1
static void update(void *arg, long period) {

	lprintk("########### Component is running\n");
}

#endif

extern double kstrtod(char *str);


int rtapp_main(void *args) {
	char buf[HAL_NAME_LEN + 1];
	int task_id, comp_id;
	int retval;
	char *cp2;

	lprintk("OpenCN RT Domain ready\n");

#if 0
	{
		double x;

		x = pow(9, 4.0);

		opencn_printf("### res = %f\n", x);

	}


#endif

#if 0 /* GSL various testings */
	{
		gsl_vector *B;
		gsl_bspline_workspace *ws;
		int i;

		lprintk("### Testing bspline ...\n");

		/* spline degree, number of break */
		ws = gsl_bspline_alloc(4, 25);

		/* x0, x1, workspace */
		gsl_bspline_knots_uniform(0.0, 1.0, ws);
		B = gsl_vector_alloc(gsl_bspline_ncoeffs(ws));

		/* x, output vector, workspace */
		gsl_bspline_eval(0.5, B, ws);

		gsl_vector_fprintf(NULL, B, "%3.2f");

		lprintk("### End of test ...\n");

	}

#endif

	char tmpstr[120];
	char result[120];
	float val1;

	val1 = 13.2454678;

	opencn_snprintf(tmpstr, 120, "%.2f ", val1);
	strcpy(result, tmpstr);

	printk("### RESULT: %s\n", result);
#if 0
	float val1, val2;
	char tmpstr[120];
	char result[120];

	//char *i1 = "-2.5283247e-03  1.2287190e-03";

	char *i1 = "-1.8736674e-59";

	val1 = strtod(i1, &cp2);

	opencn_snprintf(tmpstr, 120, "%f ", val1);
	strcpy(result, tmpstr);

	printk("### RESULT: %s\n", result);
#endif

#if 0
	{
		double check;
		//char *str = "0.0000000e+00";
		char *str = "19824.243548796758493e+01 f";

		kernel_fpu_begin();

		check = strtod(str, &cp2);
		lprintk("## value: %lu\n", (unsigned long) (check * (double) 100.0));
		lprintk("## %p %p:%d\n", str, cp2, *cp2);
		kernel_fpu_end();
	}
#endif

#if 0
	comp_id = hal_init("rtapp_try");

	rtapi_snprintf(buf, sizeof(buf), "streamer.%d", 1);
	retval = hal_export_funct(buf, update, NULL, 0, 0, comp_id);
	hal_ready(comp_id);

	comp_id = hal_init("threads");
	retval = hal_create_thread("thread1", SECONDS(1), 0);
	hal_ready(comp_id);

	hal_add_funct_to_thread("streamer.1", "thread1", -1);

	hal_start_threads();
#endif

#if 1
	//rtdm_event_init(&rt_event1, 0);

	//lprintk("########## %ld\n", (int) (try(1.2)*100));

	rtdm_task_init(&rt_task1, "rt_task_1", rt_task1_fn, NULL, 50, SECONDS(1));
	//rtdm_task_init(&rt_task2, "rt_task_2", rt_task2_fn, NULL, 50, 500000000);

	//printk("## Sending DC_ENABLE_RT now...\n");
	//do_sync_dom(OPENCN_RT_CPU, DC_ENABLE_RT);
#endif

	/* We can leave this thread die. Our system is living anyway... */
	do_exit(0);

}
