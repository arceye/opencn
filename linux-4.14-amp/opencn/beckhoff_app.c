/*
 * Copyright (C) 2014-2019 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *                         Kevin JOLY <kevin.joly@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/* Xenomai */
#include <xenomai/rtdm/driver.h>

/* EtherCAT */
#include <soo/opencn/ecrt.h>

#define PFX "BECKHOFF: "

#define BusCouplerPos   0, 0
#define El1252Pos       0, 1
#define El2252Pos       0, 2

#define Beckhoff_EK1100 0x00000002, 0x044c2c52
#define Beckhoff_EL1252 0x00000002, 0x04e43052
#define Beckhoff_EL2252 0x00000002, 0x08cc3052

#define PERIOD_US (100)
#define PERIOD_NS (MICROSECS(PERIOD_US))

#define CLOCK_CORRECTION_LIMIT_PERCENT 1ull
#define CLOCK_CORRECTION_P_GAIN_NUM 10ll
#define CLOCK_CORRECTION_P_GAIN_DEN 11ll
#define CLOCK_CORRECTION_I_GAIN_NUM 1ll
#define CLOCK_CORRECTION_I_GAIN_DEN 200ll

enum TRIGGER_STATE {
	IDLE,
	SET_TRIGGER,
	ACTIVATE_TRIGGER
};

struct clock_correction {
	int64_t limit_ns;
	uint64_t period_ns;
	int64_t isum_ns;
	int64_t out_ns;
};

/*****************************************************************************/

ec_pdo_entry_info_t el1252_entries[] = {
/*  Index,  SIndx,Len */
	{0x6000, 0x01, 1},  /* Input */
	{0x6000, 0x02, 1},  /* Input */
	{0x0000, 0x00, 6},  /* gap */
	{0x1d09, 0xae, 8},  /* Status1 */
	{0x1d09, 0xaf, 8},  /* Status2 */
	{0x1d09, 0xb0, 64}, /* LatchPos1 */
	{0x1d09, 0xb8, 64}, /* LatchNeg1 */
	{0x1d09, 0xc0, 64}, /* LatchPos2 */
	{0x1d09, 0xc8, 64}, /* LatchNeg2 */
};

ec_pdo_info_t el1252_pdos[] = {
/*   Index,  n.PDOs, array of PDO entries */
	{0x1a00, 1, el1252_entries + 0}, /* Channel 1 */
	{0x1a01, 2, el1252_entries + 1}, /* Channel 2 */
	{0x1a02, 0, NULL},                    /* Reserved */
	{0x1a13, 6, el1252_entries + 3}, /* Latches */
};

ec_sync_info_t el1252_syncs[] = {
/*  Indx, SM direction, n.PDOs, array of PDOs, WD mode */
	{0, EC_DIR_INPUT, 3, el1252_pdos + 0, EC_WD_DISABLE},
	{1, EC_DIR_INPUT, 1, el1252_pdos + 3, EC_WD_DISABLE},
	{2, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
	{0xff}
};

/*****************************************************************************/

ec_pdo_entry_info_t el2252_entries[] = {
	{0x1d09, 0x81, 8}, /* Activate */
	{0x1d09, 0x90, 64}, /* StartTime */
	{0x7000, 0x01, 1}, /* Output */
	{0x7000, 0x02, 1}, /* TriState */
	{0x7010, 0x01, 1}, /* Output */
	{0x7010, 0x02, 1}, /* TriState */
	{0x0000, 0x00, 4},
};

ec_pdo_info_t el2252_pdos[] = {
	{0x1602, 1, el2252_entries + 0}, /* DC Sync Activate */
	{0x1603, 1, el2252_entries + 1}, /* DC Sync Start */
	{0x1600, 2, el2252_entries + 2}, /* Channel 1 */
	{0x1601, 3, el2252_entries + 4}, /* Channel 2 */
	{0x1604, 0, NULL}, /* Reserved */
};

ec_sync_info_t el2252_syncs[] = {
	{0, EC_DIR_INPUT, 1, el2252_pdos + 0, EC_WD_DISABLE},
	{1, EC_DIR_INPUT, 1, el2252_pdos + 1, EC_WD_DISABLE},
	{2, EC_DIR_OUTPUT, 3, el2252_pdos + 2, EC_WD_ENABLE},
	{3, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
	{4, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
	{0xff}
};

/*****************************************************************************/

static rtdm_task_t ec_cycle_task;
static rtdm_task_t ec_init_task;

static ec_master_t *master  = NULL;
static ec_domain_t *domain1 = NULL;
static uint8_t     *domain1_pd; // process data memory

struct el1252_offsets {
	unsigned int input_1;
	unsigned int input_2;
	unsigned int status_1;
	unsigned int status_2;
	unsigned int latch_Pos1;
	unsigned int latch_neg1;
	unsigned int latch_pos2;
	unsigned int latch_neg2;
	unsigned int input_1_bit;
	unsigned int input_2_bit;
};

struct el2252_offsets {
	unsigned int activate;
	unsigned int start_time;
	unsigned int output_1;
	unsigned int output_1_bits;
	unsigned int tristate_1;
	unsigned int tristate_1_bits;
	unsigned int output_2;
	unsigned int output_2_bits;
	unsigned int tristate_2;
	unsigned int tristate_2_bits;
	unsigned int empty;
	unsigned int empty_bits;
};

static struct el1252_offsets el1252_offsets;
static struct el2252_offsets el2252_offsets;

// PDO entries of domain 1
const static ec_pdo_entry_reg_t domain1_regs[] = {
	{ El1252Pos, Beckhoff_EL1252, 0x6000, 0x01, &el1252_offsets.input_1, &el1252_offsets.input_1_bit},
	{ El1252Pos, Beckhoff_EL1252, 0x6000, 0x02, &el1252_offsets.input_2, &el1252_offsets.input_2_bit},
	{ El1252Pos, Beckhoff_EL1252, 0x1d09, 0xae, &el1252_offsets.status_1 },
	{ El1252Pos, Beckhoff_EL1252, 0x1d09, 0xaf, &el1252_offsets.status_2 },
	{ El1252Pos, Beckhoff_EL1252, 0x1d09, 0xb0, &el1252_offsets.latch_Pos1 },
	{ El1252Pos, Beckhoff_EL1252, 0x1d09, 0xb8, &el1252_offsets.latch_neg1 },
	{ El1252Pos, Beckhoff_EL1252, 0x1d09, 0xc0, &el1252_offsets.latch_pos2 },
	{ El1252Pos, Beckhoff_EL1252, 0x1d09, 0xc8, &el1252_offsets.latch_neg2 },
	{ El2252Pos, Beckhoff_EL2252, 0x1d09, 0x81, &el2252_offsets.activate },
	{ El2252Pos, Beckhoff_EL2252, 0x1d09, 0x90, &el2252_offsets.start_time },
	{ El2252Pos, Beckhoff_EL2252, 0x7000, 0x01, &el2252_offsets.output_1,   &el2252_offsets.output_1_bits },
	{ El2252Pos, Beckhoff_EL2252, 0x7000, 0x02, &el2252_offsets.tristate_1, &el2252_offsets.tristate_1_bits },
	{ El2252Pos, Beckhoff_EL2252, 0x7010, 0x01, &el2252_offsets.output_2,   &el2252_offsets.output_2_bits },
	{ El2252Pos, Beckhoff_EL2252, 0x7010, 0x02, &el2252_offsets.tristate_2, &el2252_offsets.tristate_2_bits },
	{}
};

extern int ec_init_module(void);
/*****************************************************************************/

void clock_correction_init(struct clock_correction* corr, uint64_t task_period_ns)
{
	corr->limit_ns = (int64_t)(task_period_ns * CLOCK_CORRECTION_LIMIT_PERCENT) / 100ull;
	corr->period_ns = task_period_ns;
	corr->isum_ns = 0ll;
}

int64_t clock_correction_compute(struct clock_correction* corr, uint32_t last_app_time_ns, uint32_t dc_time_ns)
{
	int err;

	err = (int)(last_app_time_ns - dc_time_ns);

	/* Anti-windup */
	if (((err > 0) && (corr->out_ns < corr->limit_ns)) ||
	    ((err < 0) && (corr->out_ns > -corr->limit_ns))) {
		corr->isum_ns += err;
	}

	corr->out_ns = (CLOCK_CORRECTION_P_GAIN_NUM * err) / CLOCK_CORRECTION_P_GAIN_DEN +
		       (CLOCK_CORRECTION_I_GAIN_NUM * corr->isum_ns *
		       (1000000000ll/(int64_t)corr->period_ns)) / CLOCK_CORRECTION_I_GAIN_DEN;

	return corr->out_ns;
}

static void el1252_read_inputs(void)
{
    uint8_t in;
    static uint8_t in1 = 0;
    static uint8_t in2 = 0;

    in = EC_READ_BIT(domain1_pd + el1252_offsets.input_1, el1252_offsets.input_1_bit);
    if (in != in1) {
        lprintk(KERN_INFO PFX "IN1 state change, current '%d'\n", in);
        in1 = in;
    }

    in = EC_READ_BIT(domain1_pd + el1252_offsets.input_2, el1252_offsets.input_2_bit);
    if (in != in2) {
        lprintk(KERN_INFO PFX "IN2 state change, current '%d'\n", in);
        in2 = in;
    }
}

static void el2252_activate_output(void)
{
	EC_WRITE_U8(domain1_pd + el2252_offsets.activate, 3);
}

static void el2252_set_output(int enable, uint64_t trigger_time)
{
	EC_WRITE_U8(domain1_pd  + el2252_offsets.activate, 0);
	EC_WRITE_U64(domain1_pd + el2252_offsets.start_time, trigger_time);
	EC_WRITE_BIT(domain1_pd + el2252_offsets.output_1, el2252_offsets.output_1_bits, enable);
	EC_WRITE_BIT(domain1_pd + el2252_offsets.output_2, el2252_offsets.output_2_bits, !enable);
}

static void cycle_task_fn(void *data)
{
	int started = 0;
	int activate = 0;
	int blink = 0;
	int dc_time_valid;
	int ret;
	uint32_t dc_time;
	uint64_t now;
	uint32_t last_app_time = 0u;
	uint64_t app_time = 0ull;
	uint64_t app_time_base;
	uint64_t nextstart;
	uint64_t dcref = 0ll;
	int64_t clock_offset = 0ll;
	struct clock_correction corr;

	clock_correction_init(&corr, PERIOD_NS);

	app_time_base = rtdm_clock_read_monotonic();
	nextstart = app_time_base;

	while (true) {

		now = rtdm_clock_read_monotonic();
		if (nextstart > now) {
			ret = rtdm_task_sleep_abs(nextstart, RTDM_TIMERMODE_ABSOLUTE);
			if (ret) {
				lprintk(KERN_ERR "rtdm_task_sleep_until: %d\n", ret);
				break;
			}
		}

		/* receive process data */
		ecrt_master_receive(master);
		ecrt_domain_process(domain1);

		/* read inputs */
		el1252_read_inputs();

		/* Set output */
		if (activate) {
			el2252_activate_output();
		} else {
			el2252_set_output(blink, app_time + PERIOD_NS);
			blink = !blink;
		}
		activate = !activate;

		ecrt_domain_queue(domain1);

		/* Set application time */
		now = rtdm_clock_read_monotonic();
		dcref += PERIOD_NS;
		app_time = app_time_base + dcref + (now - nextstart);
		ecrt_master_application_time(master, app_time);

		ret = ecrt_master_reference_clock_time(master, &dc_time);
		if (ret != 0) {
			dc_time_valid = 0;
		} else {
			dc_time_valid = 1;
		}

		ecrt_master_sync_slave_clocks(master);

		ecrt_master_send(master);

		if (started) {
			if (dc_time_valid) {
				clock_offset = clock_correction_compute(&corr,
									last_app_time,
									dc_time);
				if (clock_offset < -(int64_t)PERIOD_NS/100)
					clock_offset = -(int64_t)PERIOD_NS/100;
				if (clock_offset > (int64_t)PERIOD_NS/100)
					clock_offset = (int64_t)PERIOD_NS/100;

			} else {
				clock_offset = 0ll;
			}
		} else {
			started = 1;
		}

		last_app_time = (uint32_t)app_time;

		nextstart += PERIOD_NS + clock_offset;
	}
}

/*****************************************************************************/

static void beckhoff_app(void *data)
{
	int ret = -1;
	ec_slave_config_t *sc;

	lprintk(KERN_INFO PFX "Starting...\n");

	while (1) {
		master = ecrt_request_master(0);
		if (master) {
			lprintk(KERN_INFO PFX "Master request done\n");
			break;
		}
		rtdm_task_sleep(MILLISECS(500));
	}

	lprintk(KERN_INFO PFX "Registering domain...\n");

	domain1 = ecrt_master_create_domain(master);
	if (!domain1) {
		lprintk(KERN_ERR PFX "Domain creation failed!\n");
		goto out_release_master;
	}

	lprintk(KERN_INFO PFX "Configuring PDOs...\n");

	sc = ecrt_master_slave_config(master, BusCouplerPos, Beckhoff_EK1100);
	if (!sc) {
		lprintk(KERN_ERR PFX "Failed to get slave configuration.\n");
		goto out_release_master;
	}

	/* == EL1252 PDOs configurations == */

	sc = ecrt_master_slave_config(master, El1252Pos, Beckhoff_EL1252);
	if (!sc) {
		lprintk(KERN_ERR PFX "Failed to get slave configuration.\n");
		goto out_release_master;
	}

	if (ecrt_slave_config_pdos(sc, EC_END, el1252_syncs)) {
		lprintk(KERN_ERR PFX "Failed to configure PDOs.\n");
		goto out_release_master;
	}

	ecrt_slave_config_dc(sc, 0x0300, PERIOD_NS, 0, 0, 0);

	sc = ecrt_master_slave_config(master, El2252Pos, Beckhoff_EL2252);
	if (!sc) {
		lprintk(KERN_ERR PFX "Failed to get slave configuration.\n");
		goto out_release_master;
	}

	if (ecrt_slave_config_pdos(sc, EC_END, el2252_syncs)) {
		lprintk(KERN_ERR PFX "Failed to configure PDOs.\n");
		goto out_release_master;
	}

	ecrt_slave_config_dc(sc, 0x0300, PERIOD_NS, PERIOD_NS/2, 0, 0);

	/* Register PDOs */
	lprintk("Configuring domains with registered PDO entries...\n");
	if (ecrt_domain_reg_pdo_entry_list(domain1, domain1_regs)) {
		lprintk(KERN_ERR PFX "PDO entry registration failed for domain 1!\n");
		goto out_release_master;
	}


	lprintk(KERN_INFO PFX "Activating master...\n");
	if (ecrt_master_activate(master)) {
		lprintk(KERN_ERR PFX "Failed to activate master!\n");
		goto out_release_master;
	}


	/* Get internal process data for domain */
	domain1_pd = ecrt_domain_data(domain1);

	lprintk(KERN_INFO PFX "Starting cyclic sample thread...\n");

	ret = rtdm_task_init(&ec_cycle_task, "ec_cycle_task", cycle_task_fn, NULL, 50, 0);
	if (ret) {
		lprintk(KERN_ERR PFX "rtdm_task_init failed: %d\n", ret);
		goto out_release_master;
	}

	lprintk(KERN_INFO PFX "Initialized.\n");

	return;

out_release_master:
	ecrt_release_master(master);
}

/*****************************************************************************/

#if 0
int __init beckhoff_modeb_init(void)
#endif
int rtapp_main(void *args) {

	int ret;

	lprintk(KERN_INFO PFX "Initialization of Beckhoff module\n");

	ret = rtdm_task_init(&ec_init_task, "ec_init_task", beckhoff_app, NULL, 50, 0);
	if (ret)
		lprintk(KERN_ERR PFX "rtdm_task_init failed: %d\n", ret);

	/* We can leave this thread die. Our system is living anyway... */
	do_exit(0);

	return ret;
}

#if 0
static void __exit beckhoff_modeb_exit(void)
{
	rtdm_task_destroy(&ec_cycle_task);
	ecrt_release_master(master);
}

module_init(beckhoff_modeb_init);
module_exit(beckhoff_modeb_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>");
MODULE_DESCRIPTION("");

#endif
