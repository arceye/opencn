/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: EvalCurvStruct.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

#ifndef EVALCURVSTRUCT_H
#define EVALCURVSTRUCT_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void EvalCurvStruct(const CurvStruct *b_CurvStruct, double u_vec_data[],
    const int u_vec_size[2], double r0D_data[], int r0D_size[2], double
    r1D_data[], int r1D_size[2], double r2D_data[], int r2D_size[2], double
    r3D_data[], int r3D_size[2]);

#endif

/*
 * File trailer for EvalCurvStruct.h
 *
 * [EOF]
 */
