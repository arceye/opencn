/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedoptTypes.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

/* Include Files */
#include "FeedoptTypes.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_linspace.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * coder.ceval('dummy_fn_for_queue_generation', Q);
 * Arguments    : const CurvStruct *b_CurvStruct
 *                const FeedoptConfigStruct *FeedoptConfig
 *                QueueId Q
 *                PushStatus push_status
 * Return Type  : void
 */
void FeedoptTypes(const CurvStruct *b_CurvStruct, const FeedoptConfigStruct
                  *FeedoptConfig, QueueId Q, PushStatus push_status)
{
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    (void)b_CurvStruct;
    (void)FeedoptConfig;
    (void)Q;
    (void)push_status;
}

/*
 * File trailer for FeedoptTypes.c
 *
 * [EOF]
 */
