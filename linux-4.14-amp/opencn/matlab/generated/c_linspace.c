/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: c_linspace.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

/* Include Files */
#include "c_linspace.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_emxutil.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "sinspace.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : double x0
 *                double x1
 *                int N
 *                double A_data[]
 *                int A_size[2]
 * Return Type  : void
 */
void c_linspace(double x0, double x1, int N, double A_data[], int A_size[2])
{
    int n1;
    emxArray_real_T *A;
    int i;
    double delta1;
    int k;
    double delta2;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    n1 = N;
    if (N < 0) {
        n1 = 0;
    }

    emxInit_real_T(&A, 2);
    i = A->size[0] * A->size[1];
    A->size[0] = 1;
    A->size[1] = n1;
    emxEnsureCapacity_real_T(A, i);
    if (n1 >= 1) {
        A->data[n1 - 1] = x1;
        if (A->size[1] >= 2) {
            A->data[0] = x0;
            if (A->size[1] >= 3) {
                if ((x0 == -x1) && (n1 > 2)) {
                    i = n1 - 1;
                    for (k = 2; k <= i; k++) {
                        A->data[k - 1] = x1 * ((double)(((k << 1) - n1) - 1) /
                                               ((double)n1 - 1.0));
                    }

                    if ((n1 & 1) == 1) {
                        A->data[n1 >> 1] = 0.0;
                    }
                } else if (((x0 < 0.0) != (x1 < 0.0)) && ((fabs(x0) >
                             8.9884656743115785E+307) || (fabs(x1) >
                             8.9884656743115785E+307))) {
                    delta1 = x0 / ((double)A->size[1] - 1.0);
                    delta2 = x1 / ((double)A->size[1] - 1.0);
                    i = A->size[1];
                    for (k = 0; k <= i - 3; k++) {
                        A->data[k + 1] = (x0 + delta2 * ((double)k + 1.0)) -
                            delta1 * ((double)k + 1.0);
                    }
                } else {
                    delta1 = (x1 - x0) / ((double)A->size[1] - 1.0);
                    i = A->size[1];
                    for (k = 0; k <= i - 3; k++) {
                        A->data[k + 1] = x0 + ((double)k + 1.0) * delta1;
                    }
                }
            }
        }
    }

    A_size[0] = 1;
    A_size[1] = A->size[1];
    n1 = A->size[1];
    for (i = 0; i < n1; i++) {
        A_data[i] = A->data[i];
    }

    emxFree_real_T(&A);
}

/*
 * File trailer for c_linspace.c
 *
 * [EOF]
 */
