/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedoptTypes_initialize.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

/* Include Files */
#include "FeedoptTypes_initialize.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_linspace.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void FeedoptTypes_initialize(void)
{
    g_FeedoptConfig = r;
    isInitialized_FeedoptTypes = true;
}

/*
 * File trailer for FeedoptTypes_initialize.c
 *
 * [EOF]
 */
