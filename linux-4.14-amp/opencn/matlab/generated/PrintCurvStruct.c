/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: PrintCurvStruct.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

/* Include Files */
#include "PrintCurvStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "ResampleTick.h"
#include "c_linspace.h"
#include "sinspace.h"
#include <stdio.h>

/* Type Definitions */
#ifndef struct_emxArray_char_T_1x10
#define struct_emxArray_char_T_1x10

struct emxArray_char_T_1x10
{
    char data[10];
    int size[2];
};

#endif                                 /*struct_emxArray_char_T_1x10*/

#ifndef typedef_emxArray_char_T_1x10
#define typedef_emxArray_char_T_1x10

typedef struct emxArray_char_T_1x10 emxArray_char_T_1x10;

#endif                                 /*typedef_emxArray_char_T_1x10*/

#ifndef struct_ssdn8APItDhdZ1vTMxzM2X_tag
#define struct_ssdn8APItDhdZ1vTMxzM2X_tag

struct ssdn8APItDhdZ1vTMxzM2X_tag
{
    emxArray_char_T_1x10 f1;
};

#endif                                 /*struct_ssdn8APItDhdZ1vTMxzM2X_tag*/

#ifndef typedef_cell_wrap_5
#define typedef_cell_wrap_5

typedef struct ssdn8APItDhdZ1vTMxzM2X_tag cell_wrap_5;

#endif                                 /*typedef_cell_wrap_5*/

/* Function Definitions */

/*
 * Arguments    : const CurvStruct *S
 * Return Type  : void
 */
void PrintCurvStruct(const CurvStruct *S)
{
    char str_Value_data[9];
    int i;
    cell_wrap_5 validatedHoleFilling[1];
    char varargin_1_data[10];
    int i1;
    double dv[3][6];
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    if (g_FeedoptConfig.DebugPrint) {
        printf("CURVE STRUCT\n");
        fflush(stdout);
    }

    switch (S->Type) {
      case CurveType_Line:
        if (g_FeedoptConfig.DebugPrint) {
            printf("    Type: Line\n");
            fflush(stdout);
        }

        switch (S->ZSpdMode) {
          case ZSpdMode_NN:
            str_Value_data[0] = 'N';
            str_Value_data[1] = 'N';
            break;

          case ZSpdMode_ZN:
            str_Value_data[0] = 'Z';
            str_Value_data[1] = 'N';
            break;

          case ZSpdMode_NZ:
            str_Value_data[0] = 'N';
            str_Value_data[1] = 'Z';
            break;

          default:
            str_Value_data[0] = 'Z';
            str_Value_data[1] = 'Z';
            break;
        }

        if (g_FeedoptConfig.DebugPrint) {
            for (i = 0; i < 2; i++) {
                validatedHoleFilling[0].f1.data[i] = str_Value_data[i];
            }

            validatedHoleFilling[0].f1.data[2] = '\x00';
            for (i = 0; i < 3; i++) {
                varargin_1_data[i] = validatedHoleFilling[0].f1.data[i];
            }

            printf("ZSpdMode: %s\n", &varargin_1_data[0]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("      P0: [%.3f %.3f %.3f]\n", S->P0[0], S->P0[1], S->P0[2]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("      P1: [%.3f %.3f %.3f]\n", S->P1[0], S->P1[1], S->P1[2]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("FeedRate: %.2f\n", S->FeedRate);
            fflush(stdout);
        }
        break;

      case CurveType_Helix:
        if (g_FeedoptConfig.DebugPrint) {
            printf("    Type: Helix\n");
            fflush(stdout);
        }

        switch (S->ZSpdMode) {
          case ZSpdMode_NN:
            str_Value_data[0] = 'N';
            str_Value_data[1] = 'N';
            break;

          case ZSpdMode_ZN:
            str_Value_data[0] = 'Z';
            str_Value_data[1] = 'N';
            break;

          case ZSpdMode_NZ:
            str_Value_data[0] = 'N';
            str_Value_data[1] = 'Z';
            break;

          default:
            str_Value_data[0] = 'Z';
            str_Value_data[1] = 'Z';
            break;
        }

        if (g_FeedoptConfig.DebugPrint) {
            for (i = 0; i < 2; i++) {
                validatedHoleFilling[0].f1.data[i] = str_Value_data[i];
            }

            validatedHoleFilling[0].f1.data[2] = '\x00';
            for (i = 0; i < 3; i++) {
                varargin_1_data[i] = validatedHoleFilling[0].f1.data[i];
            }

            printf("ZSpdMode: %s\n", &varargin_1_data[0]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("      P0: [%.3f %.3f %.3f]\n", S->P0[0], S->P0[1], S->P0[2]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("      P1: [%.3f %.3f %.3f]\n", S->P1[0], S->P1[1], S->P1[2]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("    evec: [%.3f %.3f %.3f]\n", S->evec[0], S->evec[1],
                   S->evec[2]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("   theta: %.3f\n", S->theta);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("   pitch: %.3f\n", S->pitch);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("FeedRate: %.2f\n", S->FeedRate);
            fflush(stdout);
        }
        break;

      case CurveType_TransP5:
        if (g_FeedoptConfig.DebugPrint) {
            printf("    Type: TransP5\n");
            fflush(stdout);
        }

        switch (S->ZSpdMode) {
          case ZSpdMode_NN:
            str_Value_data[0] = 'N';
            str_Value_data[1] = 'N';
            break;

          case ZSpdMode_ZN:
            str_Value_data[0] = 'Z';
            str_Value_data[1] = 'N';
            break;

          case ZSpdMode_NZ:
            str_Value_data[0] = 'N';
            str_Value_data[1] = 'Z';
            break;

          default:
            str_Value_data[0] = 'Z';
            str_Value_data[1] = 'Z';
            break;
        }

        if (g_FeedoptConfig.DebugPrint) {
            for (i = 0; i < 2; i++) {
                validatedHoleFilling[0].f1.data[i] = str_Value_data[i];
            }

            validatedHoleFilling[0].f1.data[2] = '\x00';
            for (i = 0; i < 3; i++) {
                varargin_1_data[i] = validatedHoleFilling[0].f1.data[i];
            }

            printf("ZSpdMode: %s\n", &varargin_1_data[0]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf(" CoeffP5: \n");
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("| ");
            fflush(stdout);
        }

        for (i = 0; i < 3; i++) {
            for (i1 = 0; i1 < 6; i1++) {
                dv[i][i1] = S->CoeffP5[i1][i];
            }
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("%.3f ", dv[0][0]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("| ");
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("%.3f ", dv[1][0]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("| ");
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("%.3f ", dv[2][0]);
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("| ");
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("\n");
            fflush(stdout);
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("FeedRate: %.2f\n", S->FeedRate);
            fflush(stdout);
        }
        break;

      default:
        printf("!!! Type = %d, UNKNOWN !!!\n", (int)S->Type);
        fflush(stdout);
        break;
    }
}

/*
 * File trailer for PrintCurvStruct.c
 *
 * [EOF]
 */
