#include "tools.h"
#include <linux/printk.h>
#include <linux/slab.h>

void spline_base_eval(unsigned long *handle, int n_sample, double *x, spline_base_t *base) {
    base->n_sample = n_sample;
    base->n_coeff = c_bspline_ncoeff(handle);
    base->size_val[0] = base->n_sample;
    base->size_val[1] = base->n_coeff;
    base->size_integr[0] = base->n_coeff;
    base->size_integr[1] = 1;
    c_bspline_base_eval(handle, n_sample, x, base->BasisVal, base->BasisValD, base->BasisValDD, base->BasisIntegr);
}

void linspace(double *out, double x0, double x1, int n) {
    int i;
    for (i = 0; i < n; i++) {
        out[i] = x0 + (x1 - x0) / (n - 1) * i;
    }
}

void c_assert_(const char *msg) { printk(KERN_ERR "(c_assert) %s\n", msg); }

#ifdef __KERNEL__
void *malloc(size_t size) {
	return kmalloc(size, GFP_ATOMIC);
}

void* calloc(size_t nmemb, size_t size) {
	return kcalloc(nmemb, size, GFP_ATOMIC);
}

void free(void* ptr) {
	kfree(ptr);
}


#endif
