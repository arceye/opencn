﻿// The number of these two  directives must match the device's Tama Virtual Machine ID and Register Layout ID,
// respectively.
using Triamec.Tama.Vmid5;
using Triamec.Tama.Rlid19;
using Triamec.TriaLink;


[Tama]
static class TamaProgram
{
    enum ErrorReason
    {
        None = 0,
        SelfInternal = 1,
        SelfLimits = 2,
        Other = 3
    }
    enum TamaState
    {
        Initialization = 1,
        WaitStartup,
        Operational,
        WaitErrorReset,
        ResetError,
        CommitWait,
    }
    enum LimitState
    {
        LimitOk,
        LimitRight,
        LimitLeft,
        LimitUnknown
    }

    static TamaState tamaState = TamaState.Initialization;
    static LimitState limitState_Z = LimitState.LimitOk;
    static ErrorReason error_reason = ErrorReason.None;
    static int error_reset_delay_count = 0;
    const int ERROR_RESET_DELAY = 10000;
    static bool sending_error = false;
    static int commit_wait = 0;
    const int COMMIT_WAIT = 100;
    static bool do_commit = false;
    static TamaState next_state = TamaState.Initialization;

    static bool GetDigitalInAxes_0(int channel) // channel 0 correspond a In1
    {
        return ((Register.Axes_0.Signals.General.DigitalInputBits & (1 << channel)) != 0);
    }

    static void set_other_drive_ext_error(bool value)
    {

        //Register.Axes_0.Commands.General.OverwriteControlSystem = value ? 1 : 0;
        //Register.Axes_1.Commands.General.OverwriteControlSystem = value ? 1 : 0;
        if (Register.Axes_0.Commands.General.DigitalOutput0 != !value)
        {
            Register.Axes_0.Commands.General.DigitalOutput0 = !value;
            do_commit = true;
        }
    }

    static void set_self_ext_error(bool value)
    {
        Register.Axes_0.Commands.General.OverwriteControlSystem = value ? 1 : 0;
        Register.Axes_1.Commands.General.OverwriteControlSystem = value ? 1 : 0;
        Register.General.Commands.ExternalError = value;
        do_commit = true;
    }

    [TamaTask(Task.IsochronousMain)]
    static void Main()
    {
        bool FinCourseZ = !GetDigitalInAxes_0(0) ||
            Register.Axes_0.Signals.PositionController.Encoders_1.Position < -20 ||
            Register.Axes_0.Signals.PositionController.Encoders_1.Position > 30;

        bool other_drive_error = !GetDigitalInAxes_0(1);
        DeviceState driveState = Register.General.Signals.DriveState;
        DeviceErrorIdentification driveError = Register.General.Signals.DriveError;
        AxisState axisState_0 = Register.Axes_0.Signals.General.AxisState;
        AxisState axisState_1 = Register.Axes_1.Signals.General.AxisState;
        AxisErrorIdentification axisError_0 = Register.Axes_0.Signals.General.AxisError;
        AxisErrorIdentification axisError_1 = Register.Axes_1.Signals.General.AxisError;

        sending_error = !Register.Axes_0.Commands.General.DigitalOutput0;
        Register.Application.Variables.Booleans[1] = FinCourseZ;
        Register.Application.Variables.Booleans[2] = other_drive_error;
        Register.Application.Variables.Booleans[3] = sending_error;
        Register.Application.Variables.Integers[5] = (int)error_reason;
        Register.Application.Variables.Integers[6] = error_reset_delay_count;

        bool limit_switch_error = false;

        // Fin de course axe Z
        switch (limitState_Z)
        {
            case LimitState.LimitOk:
                Register.Application.Variables.Integers[1] = 0; // debug state
                if (FinCourseZ && Register.Axes_0.Signals.Homing.State != HomingState.FirstSearch && Register.Axes_0.Signals.Homing.State != HomingState.RelocationMove)
                {
                    if (Register.Axes_0.Signals.PathPlanner.Velocity < 0)
                    {
                        limitState_Z = LimitState.LimitLeft;
                    }
                    else if (Register.Axes_0.Signals.PathPlanner.Velocity > 0)
                    {
                        limitState_Z = LimitState.LimitRight;
                    }
                    else
                    {
                        limitState_Z = LimitState.LimitUnknown;
                    }
                    limit_switch_error = true;
                }
                break;
            case LimitState.LimitRight:
                Register.Application.Variables.Integers[1] = 1; // debug state
                if (!FinCourseZ)
                {
                    limitState_Z = LimitState.LimitOk;
                }
                else
                {
                    if (Register.Axes_0.Signals.PathPlanner.Velocity > 0)
                    {
                        limit_switch_error = true;
                    }
                }
                break;
            case LimitState.LimitLeft:
                Register.Application.Variables.Integers[1] = 2; // debug state
                if (!FinCourseZ)
                {
                    limitState_Z = LimitState.LimitOk;
                }
                else
                {
                    if (Register.Axes_0.Signals.PathPlanner.Velocity < 0)
                    {
                        limit_switch_error = true;
                    }
                }
                break;
            case LimitState.LimitUnknown:
                Register.Application.Variables.Integers[1] = 3; // debug state
                if (!FinCourseZ)
                {
                    limitState_Z = LimitState.LimitOk;
                }
                break;
            default:
                break;
        }

        //if (driveError == DeviceErrorIdentification.None && driveState == DeviceState.Operational)
        //{
        //    tamaState = TamaState.Operational;
        //}

        //if (driveState == DeviceState.Operational)
        //{
        //    set_other_drive_ext_error(false);
        //}

        Register.Application.Variables.Integers[3] = (int)tamaState;

        // Gestion des erreurs
        switch (tamaState)
        {
            case TamaState.Initialization:

                tamaState = TamaState.WaitStartup;
                error_reset_delay_count = ERROR_RESET_DELAY;
                set_other_drive_ext_error(false);
                set_self_ext_error(false);
                break;
            case TamaState.WaitStartup:

                if (!other_drive_error && error_reset_delay_count <= 0 && driveState == DeviceState.Operational)
                {
                    tamaState = TamaState.Operational;
                }
                set_other_drive_ext_error(false);
                break;

            case TamaState.Operational:
                if ((driveError > DeviceErrorIdentification.None && driveError != DeviceErrorIdentification.LinkPllNotLocked) ||
                    axisError_0 > AxisErrorIdentification.None ||
                    axisError_1 > AxisErrorIdentification.None)
                {

                    tamaState = TamaState.WaitErrorReset;
                    error_reason = ErrorReason.SelfInternal;
                    error_reset_delay_count = ERROR_RESET_DELAY;
                    set_other_drive_ext_error(true);
                }
                else if (other_drive_error)
                {
                    error_reason = ErrorReason.Other;

                    tamaState = TamaState.WaitErrorReset;
                    error_reset_delay_count = ERROR_RESET_DELAY;
                    set_self_ext_error(true);
                    set_other_drive_ext_error(true);

                }
                else if (limit_switch_error)
                {
                    error_reason = ErrorReason.SelfLimits;

                    tamaState = TamaState.WaitErrorReset;
                    error_reset_delay_count = ERROR_RESET_DELAY;
                    set_self_ext_error(true);
                    set_other_drive_ext_error(true);

                }
                break;
            case TamaState.WaitErrorReset:
                if (error_reset_delay_count <= 0)
                {

                    tamaState = TamaState.ResetError;
                    set_self_ext_error(false);
                    set_other_drive_ext_error(false);
                }
                break;
            case TamaState.ResetError:
                if (driveState == DeviceState.ReadyToSwitchOn || driveState == DeviceState.Operational || driveState == DeviceState.NotReadyToSwitchOn)
                {
                    error_reason = ErrorReason.None;
                    tamaState = TamaState.Initialization;
                }
                break;
            case TamaState.CommitWait:
                if (--commit_wait <= 0)
                {
                    tamaState = next_state;
                }
                break;
            default:
                break;
        }
        if (error_reset_delay_count > 0) error_reset_delay_count--;
        if(do_commit)
        {
            next_state = tamaState;
            tamaState = TamaState.CommitWait;
            Register.General.Commands.CommitParameter = true;
            do_commit = false;
            commit_wait = COMMIT_WAIT;
        }
    }
}
