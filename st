#!/bin/bash

QEMU_VERSION=3.0.0
QEMU_DIR=qemu-${QEMU_VERSION}

#create a random mac address
#bash is necessary for the random function
export QEMU_MAC_ADDR="$(printf 'DE:AD:BE:EF:%02X:%02X\n' $((RANDOM%256)) $((RANDOM%256)))"
export QEMU_AUDIO_DRV="none"

./tools/${QEMU_DIR}/x86_64-softmmu/qemu-system-x86_64 $@ \
	-cpu Nehalem \
	-smp 4 \
	-serial mon:stdio \
	-netdev user,id=n1 \
	-device e1000e,netdev=n1,mac=00:01:29:53:97:BA \
	-hda rootfs/rootfs.img \
	-s \
	-m 1024 \
	-nographic \
	-kernel linux-4.14-amp/arch/x86/boot/bzImage \
	-append "root=/dev/ram console=ttyS0 earlyprintk=pciserial" \
    	-object filter-dump,id=f1,netdev=n1,file=dump.dat

