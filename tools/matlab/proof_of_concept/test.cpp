#include <ClpSimplex.hpp>
#include <CoinBuild.hpp>
#include <CoinHelperFunctions.hpp>
#include <CoinModel.hpp>
#include <CoinTime.hpp>
int main()
{
    CoinPackedMatrix matrix;
    ClpSimplex model;

    model.setLogLevel(0);

    model.loadProblem(matrix, nullptr, nullptr, nullptr, nullptr, nullptr);
    model.setPrimalTolerance(1e-6);
    model.setDualTolerance(1e-6);

    model.initialDualSolve();

    model.dual();
    return 0;
}
