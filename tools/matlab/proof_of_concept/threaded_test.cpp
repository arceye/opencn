#include "threaded_test.hpp"
extern "C" {
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedratePlanning_v4.h"
#include "SmoothCurvStructs.h"
#include "SmoothCurvStructs_emxAPI.h"
#include "c_linspace.h"
}
#include "tools.hpp"
#include <atomic>
#include <chrono>
#include <cstdarg>
#include <mutex>
#include <queue>
#include <thread>

#include <SFML/Graphics.hpp>

#include "time_prof.h"

#define CONSTR_POINTS 0
#define CONSTR_U 1
#define U_IN_PLANE 1

#define MIN(a, b) (a < b ? a : b)
#define MAX(a, b) (a > b ? a : b)

using namespace std::chrono_literals;

auto now() { return std::chrono::high_resolution_clock::now(); }

static const int N_COEFF_MAX = 120;
static const int MAX_QUEUE = 5;
static const int MAX_DISCR = 200;

static const int Ndiscr = 100;
static const int nbreak = 25;
static const double cutoff = 0.2;
static const int Nhorz = 3;

static const double eps = 1e-6;

static std::atomic<double> vmax = 1.5;
static const double amax_ = 4.5;
static const double amax[3] = {amax_, amax_, amax_};

static const double jmax_ = 100;
static const double jmax[3] = {jmax_, jmax_, jmax_};

static void rt_worker(int tick_us);
static void us_worker();
static std::atomic_bool should_stop{false};
static std::atomic_bool rt_paused{true}, us_paused{true}, rt_step{false}, us_step{false};
static int curv_queue_size;
static double current_x, current_y, current_z;
static double current_v{0};
static const char *current_type{nullptr};
static int iteration{0};

static std::mutex queue_mutex;

static struct GUI {
    sf::RenderWindow window;
    int w, h;
    sf::Font f;
    sf::Text u;
    sf::CircleShape circle;
} gui;

// check a >= b with eps tolerance
static bool ge_eps(double a, double b) { return a >= b - eps; }

static void text(int x, int y, const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    char buffer[255];
    gui.u.setPosition(x, y);
    vsprintf(buffer, fmt, va);
    va_end(va);
    gui.u.setString(buffer);
    gui.window.draw(gui.u);
}

// assumed double v[3]
static double norm(double *v) { return sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]); }
static double dot(double *v1, double *v2) { return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]; }

static float current_u = 0;
static float current_t = 0;

struct OptimizedCurvStruct {
    CurvStruct curve_;
    double coeffs_[N_COEFF_MAX];
    int coeff_count_;
    OptimizedCurvStruct(CurvStruct curve, double *coeff_data, int coeff_count)
        : curve_{curve}, coeff_count_{coeff_count}
    {
        std::copy(coeff_data, coeff_data + coeff_count, coeffs_);
    }

    OptimizedCurvStruct() { curve_.Type = 0; }
};

static std::queue<OptimizedCurvStruct> queue;

void gui_worker();

void threaded_test()
{
    gui.window.create({800, 600}, "OpenCN Test");
    gui.f.loadFromFile("/usr/share/fonts/truetype/hack/Hack-Regular.ttf");
    gui.u.setFont(gui.f);

    PROF_START("/tmp/threaded_test.prof");
    PROF_IN(threaded_test);
    int rt_tick_us = 1000000 / 1000;
    std::thread rt_thread{rt_worker, rt_tick_us};
    std::thread us_thread{us_worker};

    gui.window.setVerticalSyncEnabled(true);

    gui_worker();
    should_stop = true;

    rt_thread.join();
    us_thread.join();
    PROF_OUT(threaded_test);
    PROF_END();
}

void gui_worker()
{
    const auto line_color_base = sf::Color::White;
    const auto line_color_opt = sf::Color::Green;
    std::vector<sf::Vertex> vertices_base, vertices_opt;
    sf::View view;
    sf::View default_view = gui.window.getDefaultView();
    gui.window.setVerticalSyncEnabled(false);
    gui.window.setFramerateLimit(60);
    while (gui.window.isOpen()) {
        sf::Event ev;
        while (gui.window.pollEvent(ev)) {
            switch (ev.type) {
            case sf::Event::Closed:
                gui.window.close();
                break;
            case sf::Event::KeyPressed:
                switch (ev.key.code) {
                case sf::Keyboard::Q:
                    gui.window.close();
                    break;
                case sf::Keyboard::R:
                    // toggle paused for realtime
                    rt_paused = !rt_paused;
                    break;
                case sf::Keyboard::F:
                    // step for realtime
                    rt_step = true;
                    break;
                case sf::Keyboard::U:
                    // toggle paused for userspace
                    us_paused = !us_paused;
                    break;
                case sf::Keyboard::J:
                    // step for userspace
                    us_step = true;
                    break;
                case sf::Keyboard::Add:
                    vmax = vmax + 0.1;
                    break;
                case sf::Keyboard::Subtract:
                    vmax = vmax - 0.1;
                    break;
                }
                break;
            case sf::Event::Resized:
                //                default_view.setSize(ev.size.width, ev.size.height);
                default_view.reset(sf::FloatRect(0, 0, ev.size.width, ev.size.height));
                gui.w = ev.size.width;
                gui.h = ev.size.height;
                break;
            }
        }

        float ratio = (float)gui.w / (float)gui.h;
        view.reset(sf::FloatRect(-1, 2, 3 * ratio, -3));
        gui.window.setView(view);
        gui.window.clear(sf::Color(20, 20, 20));
        //        gui.window.draw(vertices_base.data(), vertices_base.size(),
        //        sf::PrimitiveType::LineStrip); gui.window.draw(vertices_opt.data(),
        //        vertices_opt.size(), sf::PrimitiveType::LineStrip);

        gui.circle.setRadius(0.03);
        gui.circle.setFillColor(sf::Color::Red);
        gui.circle.setPosition(current_x, current_y);
        gui.circle.setOrigin(0.03, 0.03);
        gui.window.draw(gui.circle);

        gui.window.setView(default_view);

        text(10, 10, "u = %4.1f", current_u);
        text(10, 40, "t = %4.1f", current_t);
        text(10, 70, "RT: %s", rt_paused ? "Paused" : "Running");
        text(10, 100, "US: %s", us_paused ? "Paused" : "Running");
        text(10, 130, "Queue: %d", curv_queue_size);
        //        text(10, 160, "v = %.2f", current_v);
        //        text(10, 190, "at_0 = %.1f", at_0);
        text(10, 220, "Type: %s", current_type);
        text(10, 250, "Iteration: %d", iteration);
        text(10, 280, "vmax = %.1f", vmax.load());
        text(10, 310, "amax = %.1f", amax_);
        text(10, 340, "jmax = %.1f", jmax_);

        gui.window.display();
    }
}

void print_segment(const CurvStruct &s)
{
    printf("Segment: %s", type_to_str(s.Type));
    switch (s.Type) {
    case Line:
        printf(", P0 = ");
        print_vector(s.P0, 3);
        printf(", P1 = ");
        print_vector(s.P1, 3);
        break;
    case TransP5:
        printf("coeffs = ");
        print_vector(s.CoeffP5, 18);
        break;
    }
    printf("\n");
}

void rt_worker(int tick_us)
{
    pthread_setname_np(pthread_self(), "rt_worker");
    //    std::this_thread::sleep_for(1s);
    const double x0 = 0.0, x1 = 1.0;
    bspline_t *spline_handle;
    c_bspline_create(reinterpret_cast<uint64_t *>(&spline_handle), x0, x1, 4, nbreak);
    const int Ncoeff = gsl_bspline_ncoeffs(spline_handle->ws);

    double u[Ndiscr];
    int size_u[2] = {1, Ndiscr};
    //    emxArray_real_T *u = emxCreate_real_T(0, 0);
    c_linspace(x0, x1, Ndiscr, u, size_u);
    SplineBase base;
    spline_base_eval(reinterpret_cast<uint64_t *>(&spline_handle), Ndiscr, u, base);

    PROF_BLOCK(rt_worker);
    auto t0 = now();
    auto last_segment_start = now();

    OptimizedCurvStruct current_struct;
    struct0_T Bl;
    Bl.n = base.n_coeff;
    Bl.handle = (uint64_t)spline_handle;
    double u_cell[10000];
    int u_cell_size[2];

    double uk = 0.0;
    double t = 0;
    int u_size = 0;

    double X[3] = {0, 0, 0};
    const double dt = tick_us * 1e-6;

    double r0D[3];
    double r1D[3];
    double r2D[3];
    double r3D[3];

    int r0D_size[2];
    int r1D_size[2];
    int r2D_size[2];
    int r3D_size[2];
    int u_eval_size[2] = {1, 1};

    std::queue<OptimizedCurvStruct> local_queue;

    bool buffer_underrun = false;

    while (!should_stop) {
        if (!rt_paused || rt_step) {
            {
                std::lock_guard guard{queue_mutex};
                while (local_queue.size() < MAX_QUEUE && !queue.empty()) {
                    local_queue.push(queue.front());
                    queue.pop();
                }
            }
            if (current_struct.curve_.Type == 0 || ge_eps(uk, 1) || rt_step) {
                if (!local_queue.empty()) {
                    current_struct = local_queue.front();
                    //                    print_segment(current_struct);
                    t = 0;
                    uk = 0;
                    local_queue.pop();
                    buffer_underrun = false;
                } else if (!rt_step) {
                    current_struct.curve_.Type = 0;
                    if (!buffer_underrun) {
                        buffer_underrun = true;
                        printf("BUFFER UNDERRUN!\n");
                        fflush(stdout);
                    }
                }
                rt_step = false;
            }

            if (current_struct.curve_.Type != 0) {
                t += dt;
            }

            if (current_struct.curve_.Type != 0) {
                EvalCurvStruct(&current_struct.curve_, &uk, u_eval_size, r0D, r0D_size, r1D,
                               r1D_size, r2D, r2D_size, r3D, r3D_size);
                current_x = r0D[0];
                current_y = r0D[1];
                current_z = r0D[2];
                current_v = norm(r1D);
                c_bspline_eval(&Bl.handle, current_struct.coeffs_, uk, X);
                uk = (uk + X[1] * (dt * dt) / 4.0) + std::sqrt(X[0]) * dt;
            }
        }

        current_u = uk;
        current_t = t;
        current_type = type_to_str(current_struct.curve_.Type);

        std::this_thread::sleep_until(t0 + std::chrono::microseconds(tick_us));
        t0 += std::chrono::microseconds(tick_us);
    }
}

void us_worker()
{
    pthread_setname_np(pthread_self(), "us_worker");
    PROF_BLOCK(us_worker);

    double v_0 = 0.1;
    double at_0 = 0.0;
    double v_1 = 0.1;
    double at_1 = 0.0;

    SplineBase base;

    std::vector<CurvStruct> curv_structs;
    std::vector<CurvStruct> opt_curv_structs;
    double Coeff[N_COEFF_MAX];
    int coeff_size[2];
    double u[MAX_DISCR];
    int size_u[2];
    double pitch = 0;

#if CONSTR_POINTS
    curv_structs.resize(Npoints - 1);
    for (int i = 0; i < Npoints - 1; i++) {
        ConstrLineStruct(points_data + (3 * i), points_data + (3 * (i + 1)), vmax,
                         &curv_structs[i]);
    }
    int input_segment_size[2] = {1, Npoints - 1};
#elif CONSTR_U
    double P0[3] = {0, 0, 0};
    double P1[3] = {1, 0, 0};
    double P2[3] = {1, 1, 0};
    double P3[3] = {0, 1, 0};
#if !U_IN_PLANE
    P2[2] = 1;
    P3[3] = 1;
    pitch = 2;
#endif
    double evec[3] = {0, 0, 1};
    double theta = M_PI;
    curv_structs.resize(4);
    ConstrLineStruct(P0, P1, vmax, &curv_structs[0]);
    ConstrHelixStruct(P1, P2, evec, theta, -pitch, vmax, &curv_structs[1]);
    ConstrLineStruct(P2, P3, vmax, &curv_structs[2]);
    ConstrLineStruct(P3, P0, vmax, &curv_structs[3]);

    int input_segment_size[2] = {1, 4};
#endif

    /****************** SPLINE BASE ********************/
    const double x0 = 0.0, x1 = 1.0;
    bspline_t *spline_handle;
    c_bspline_create(reinterpret_cast<uint64_t *>(&spline_handle), x0, x1, 4, nbreak);

    c_linspace(x0, x1, Ndiscr, u, size_u);

    {
        PROF_BLOCK(spline_base_eval);
        spline_base_eval(reinterpret_cast<uint64_t *>(&spline_handle), Ndiscr, u, base);
    }
    /****************** SPLINE BASE *********************/

    int output_segment_size[2];

    CurvStruct tmp_opt_in[3];
    CurvStruct tmp_opt_out[3];

    struct0_T Bl;
    Bl.n = base.n_coeff;
    Bl.handle = (uint64_t)spline_handle;

    bool first_segment = true;

    while (!should_stop) {
        int queue_size;
        {
            std::lock_guard guard{queue_mutex};
            queue_size = queue.size();
            curv_queue_size = queue_size;
        }

        while ((!us_paused || us_step) && queue_size < MAX_QUEUE) {
            us_step = false;
            while (opt_curv_structs.size() < Nhorz) {
                if (first_segment) {
                    tmp_opt_in[0] = curv_structs[0];
                    tmp_opt_in[1] = curv_structs[1];

                    input_segment_size[0] = 1;
                    input_segment_size[1] = 2;
                    SmoothCurvStructs(tmp_opt_in, input_segment_size, cutoff, vmax, tmp_opt_out,
                                      output_segment_size);

                    for (int i = 0; i < 2; i++) {
                        opt_curv_structs.push_back(tmp_opt_out[i]);
                    }

                    first_segment = false;
                } else {
                    tmp_opt_in[0] = curv_structs[0];
                    tmp_opt_in[1] = curv_structs[1];
                    tmp_opt_in[2] = curv_structs[2];

                    CurvStruct front = curv_structs[0];
                    curv_structs.erase(begin(curv_structs));
                    curv_structs.push_back(front);

                    input_segment_size[0] = 1;
                    input_segment_size[1] = 3;
                    SmoothCurvStructs(tmp_opt_in, input_segment_size, cutoff, vmax, tmp_opt_out,
                                      output_segment_size);

                    for (int i = 2; i < 4; i++) {
                        opt_curv_structs.push_back(tmp_opt_out[i]);
                    }
                }
            }
            //            vertices_base.clear();
            //            for (int k = 0; k < curv_structs.size() - 1; k++) {

            //                EvalCurvStruct(&curv_structs[k], u, size_u, r0D, r0D_size, r1D,
            //                r1D_size, r2D,
            //                               r2D_size, r3D, r3D_size);
            //                for (int i = 0; i < r0D_size[1]; i++) {
            //                    vertices_base.emplace_back(sf::Vector2f(r0D[3 * i + 0], r0D[3 * i
            //                    + 1]),
            //                                               line_color_base);
            //                }
            //            }

            //            vertices_opt.clear();
            //            for (int k = 0; k < MIN(opt_curv_structs.size() - 1, Nhorz); k++) {

            //                EvalCurvStruct(&opt_curv_structs[k], u, size_u, r0D, r0D_size, r1D,
            //                r1D_size, r2D,
            //                               r2D_size, r3D, r3D_size);
            //                for (int i = 0; i < r0D_size[1]; i++) {
            //                    vertices_opt.emplace_back(sf::Vector2f(r0D[3 * i + 0], r0D[3 * i +
            //                    1]),
            //                                              line_color_opt);
            //                }
            //            }
            output_segment_size[0] = 1;
            output_segment_size[1] = opt_curv_structs.size();
            iteration += 1;
            //            printf("Optimizing %s with v_0 = %.2f, at_0 = %+6.2f\n\t",
            //                   type_to_str(opt_curv_structs[0].Type), v_0, at_0);
            //            print_segment(opt_curv_structs[0]);
            //            printf("\n");

            FeedratePlanning_v4(                              //
                opt_curv_structs.data(), output_segment_size, //
                amax, jmax, &v_0, &at_0, v_1, at_1,           //
                base.BasisVal, base.size_val,                 //
                base.BasisValD, base.size_val,                //
                base.BasisValDD, base.size_val,               //
                base.BasisIntegr, base.size_integr,           //
                &Bl, u, size_u, Nhorz, Coeff, coeff_size);
            //            double end_u[] = {1};
            //            int end_u_size[2] = {1, 1};
            //            EvalCurvStruct(&opt_curv_structs[0], end_u, end_u_size, r0D, r0D_size,
            //            r1D, r1D_size,
            //                           r2D, r2D_size, r3D, r3D_size);
            //            v_0 = norm(r1D);
            //            double tang_end[3] = {r1D[0] / v_0, r1D[1] / v_0, r1D[2] / v_0};
            //            at_0 = dot(r2D, tang_end);
            //            printf("spline coeffs: ");
            //            print_vector(Coeff, coeff_size[0]);
            //            printf("\n");
            {
                std::lock_guard guard{queue_mutex};

                queue.emplace(opt_curv_structs[0], Coeff, coeff_size[0]);
                queue_size = queue.size();
                opt_curv_structs.erase(begin(opt_curv_structs));
            }
            //            printf("PUSH_CURVE_STRUCT\n");
            //            fflush(stdout);
        }
    }
}
